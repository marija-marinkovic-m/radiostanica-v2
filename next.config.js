const webpack = require('webpack');
module.exports = {
  webpack: (config) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    }
    config.plugins.push(
      new webpack.EnvironmentPlugin({
        'API_URL': 'http://api.radio.etl.yt',
        'STATIONS_PER_PAGE': '5',
        'I18N_URL': null,
        'PLAYER_STORAGE_KEY': 'RadiostanicaPlayerPrefs',
        'FAVORITES_STORAGE_KEY': 'RadiostanicaFavorites',
        'HISTORY_STORAGE_KEY': 'RadiostanicaHistory',
        'STORAGE_STATIONS_LIMIT': '10',
        'PLAYER_QUERY_VAR': 'play',
        'DEFAULT_NATIONALITY': 'srbija'
      })
    );

    return config;
  }
};
