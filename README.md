# Radiostanica.com

This project was build with React, Node.js. Redux + Saga are used for application state management. Frontend scaffolded with MUI. Root contains `components`, `core` and `pages` folders with respective modules.

### `npm start` or `yarn start`

Runs the app in development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will automatically reload if you make changes to the code.<br>
You will see the build errors and lint warnings in the console.

------------------

* [COMPONENTS docs](./COMPONENTS.md)
* [CORE docs](./CORE.md)