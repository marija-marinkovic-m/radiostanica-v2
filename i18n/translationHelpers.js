/* global fetch */
import es6promise from 'es6-promise';
import 'isomorphic-unfetch';
es6promise.polyfill();

export async function getTranslation(lang, files) {
  let translation = {};

  const translationsPath = process.env.I18N_URL || (typeof window !== "undefined" ? `${window.location.origin}/static/locales` : `http://localhost:${process.env.PORT || 3000}/static/locales`);

  // note: 
  // translation files should be fetched from this server
  // hence origin contant
  // if there was specific process.env.I18N_URl
  // translations shoud be fetched from that url

  for (let file of files) {
    const response = await fetch(`${translationsPath}/${lang}/${file}.json`);
    translation[file] = await response.json();
  }

  return {[lang]: translation};
}