/**
 * Cookies Module is used for storing and parsing cookies both on the client and on the server side.
 * 
 * Utility functions from `/util/cookies.js` redirect every cookie request to cookie service
 * based on the component's (initial state) context
 * to `/util/cookie-client.js` and `/util/cookie-server.js` respectively
 * @module cookies
 */
export const cookiesActionTypes = {
  SET_COOKIE: 'SET_COOKIE',
  GET_COOKIE: 'GET_COOKIE',
  COOKIE_SUCCESS: 'COOKIE_SUCCESS',
  COOKIE_FAILURE: 'COOKIE_FAILURE'
};

/** 
 * Set cookie action
 * @param {object} ctx - react component context (next)
 * @param {string} key - cookie storage key
 * @param {string} value - cookie storage value
 * @param {number} days - expiration indicator
 * */
export const setCookie = (ctx = null, key = 'theme', value = 'light', days = 30) => ({
  type: cookiesActionTypes.SET_COOKIE,
  ctx, key, value, days
});

/**
 * Get cookie action
 * @param {object} ctx - next react component context (from getInitialProps)
 * @param {string} key - cookie name (string)
 * @param {string} defValue - if cookie doesn't exist
 * */
export const getCookie = (ctx, key = 'theme', defValue = 'light') => ({
  type: cookiesActionTypes.GET_COOKIE,
  ctx, key, defValue
});

/**
 * Cookie success
 * @param {string} key - ie. 'theme'
 * @param {string} value - ie. 'light', 'dark',...
 */
export const cookieSuccess = (key = 'theme', value = 'light') => ({
  type: cookiesActionTypes.COOKIE_SUCCESS,
  key, value
});

/**
 * Cookie failure
 * @param {string} error - reason for failure
 */
export const cookieFailure = (error) => ({
  type: cookiesActionTypes.COOKIE_FAILURE,
  error
});