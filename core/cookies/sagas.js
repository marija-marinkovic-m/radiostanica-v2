/**
 * @module
 * @alias module:cookies
 */
import { put, takeLatest } from 'redux-saga/effects';
import { cookiesActionTypes as actionTypes, cookieSuccess, cookieFailure, setCookie } from './actions';

import { parseCookie, setCookie as setCookieUtility } from '../../util/cookies';
import { setTranslation } from '../translations/actions';

/**
 * This saga checks request context (server / client)
 * to store cookie by key to cookie storage
 * @param {object} data - has properties `ctx`, `key`, `value`, `days`
 */
function* setCookieSaga({ ctx, key, value, days }) {
  try {
    setCookieUtility(ctx, key, value, days);
    yield put(cookieSuccess(key, value));

    if (key === 'lang') {
      yield put(setTranslation(value));
    }

  } catch (e) {
    yield put(cookieFailure(`${e.name}: ${e.message}`));
  }
}

/**
 * This saga checks request context (server / client)
 * to fetch cookie by key from cookie storage
 * @param {object} data - has properties `ctx`, `key`, `defValue`
 */
function* getCookieSaga({ ctx, key, defValue }) {
  try {
    const allCookies = parseCookie(ctx);
    const currentValue = allCookies[key];

    if (!currentValue) {
      yield put(setCookie(ctx, key, defValue));
    } else {
      yield put(cookieSuccess(key, currentValue));
    }

    if (key === 'lang') {
      const translationLang = currentValue || defValue;
      yield put(setTranslation(translationLang));
    }

  } catch (e) {
    yield put(cookieFailure(`${e.name}: ${e.message}`));
  }
}


export default [
  takeLatest(actionTypes.SET_COOKIE, setCookieSaga),
  takeLatest(actionTypes.GET_COOKIE, getCookieSaga)
];