import slugify from '../util/slugify';

export const normalizeNationality = (data) => {
  /**
   * data: {
     "id": 4,
     "iso_code": "BA",
     "active": 1,
     "country_id": 70,
     "lang": [{
         "name": "Bosnia and Herzegovina",
         "url_name": "bih"
       },
       {
         "name": "Bosna i Hercegovina",
         "url_name": "bih"
       }
     ]
   }
   */
  const lang = Boolean(data.lang && data.lang[1]) ? data.lang[1].url_name : null;
  const name = Boolean(data.lang && data.lang[1]) ? data.lang[1].name : null;
  return {
    id: data.id,
    iso: data.iso_code,
    lang,
    name
  };
}

export const normalizeCity = (data) => {
  /**
   *
   data: {
     "id": 2,
     "name": "Blace",
     "active": 1,
     "country_id": 1,
     "nationality_id": 1,
     "lat": 43.280128,
     "lng": 21.279505,
     "nationality": {
       "id": 1,
       "iso_code": "RS",
       "active": 1,
       "country_id": 688,
       "lang": [{
           "name": "Serbia",
           "url_name": "srbija"
         },
         {
           "name": "Srbija",
           "url_name": "srbija"
         }
       ]
     }
   }
   */
  const nationality = Boolean(data.nationality && data.nationality.lang && data.nationality.lang[1]) ? data.nationality.lang[1].url_name : null
  return {
    id: data.id,
    name: data.name,
    nationalityId: data.nationality_id,
    slug: slugify(data.name),
    nationality
  }
}