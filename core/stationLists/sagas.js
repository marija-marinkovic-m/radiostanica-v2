import { call, put, select, takeEvery } from 'redux-saga/effects';
import { stationListActionTypes as actionTypes } from './actions';

import { stationActionTypes, visitStationSuccess } from '../stations/actions';

import { getStations } from '../api/actions';
import { api } from '../api/api-service';
import { getCurrentStationList } from './selectors';

export function * loadNextStations() {
  const stationList = yield select(getCurrentStationList);
  if (stationList.nextUrlParams) {
    yield put(getStations(stationList.id, stationList.nextUrlParams));
  }
}

export function * visitStationSaga({payload}) {
  try {
    const response = yield call(api.countVisit, {
      method: 'POST',
      body: {
        radiostation_id: payload.stationId
      },
      headers: {
        'Content-Type': 'application/json'
      }
    });
    yield put(visitStationSuccess(payload.stationId, {
      latestVisit: {
        response,
        timestamp: (new Date()).getTime()
      }
    }));
  } catch (e) {
    console.log('Error while trying to ping station visit', e);
  }
}

export default [
  takeEvery(actionTypes.LOAD_NEXT_STATIONS, loadNextStations),
  takeEvery(stationActionTypes.VISIT_STATION, visitStationSaga)
];