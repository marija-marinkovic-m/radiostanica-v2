import { SESSION_STATIONLIST_ID } from '../constants';
import { stationListActionTypes as actionTypes } from './actions';
import { StationList, stationListReducer } from './stationList';

export const stationListsExampleInitialState = {
  currentStationListId: SESSION_STATIONLIST_ID,
  [SESSION_STATIONLIST_ID]: ({...StationList, id: SESSION_STATIONLIST_ID, isNew: false})
};

export default function stationListsReducer(state = stationListsExampleInitialState, action) {
  const { type, payload } = action;

  switch (type) {
    case actionTypes.FETCH_STATIONS_SUCCESS:
    case actionTypes.FETCH_STATIONS_PENDING:
    case actionTypes.LOAD_STATIONLIST:
      return {
        ...state,
        currentStationListId: payload.stationListId,
        [payload.stationListId]: stationListReducer(state[payload.stationListId], action)
      };

    default:
      return state;
  }
}