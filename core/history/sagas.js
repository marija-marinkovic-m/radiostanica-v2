import { select, put, takeLatest } from 'redux-saga/effects';

import { HISTORY_STORAGE_KEY, STORAGE_STATIONS_LIMIT } from '../constants';
import { historyActionTypes as actionTypes } from './actions';
import { getHistory } from './selectors';
import { setLocalData } from '../localStorage/actions';


export function* addHistorySaga({payload: {station}}) {
  const history = yield select(getHistory);
  // check if entry exists in history
  // if entry exists move to the top
  // else prepend entry to top

  const keys = Object.keys(history);
  const exists = keys.indexOf(station.id) > -1;
  const limit = !exists ? STORAGE_STATIONS_LIMIT - 1 : keys.length;

  const value = keys
    .slice(0, limit)
    .filter(id => id !== station.id)
    .reduce((acc, next) => ({...acc, [next]: history[next]}), {
      [station.id]: station
    });

  yield put(setLocalData({
    key: HISTORY_STORAGE_KEY,
    value
  }));
}

export default [
  takeLatest(actionTypes.ADD_HISTORY, addHistorySaga)
];