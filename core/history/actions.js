/**
 * Module History is responsible for storing (predefined) number of stations to localStorage.
 * This number is sent to the module as environment variable.  
 * 
 * Newest stations are `unshifted` to the begining of history array.
 * The oldest are poped out.
 * @module history
 */

export const historyActionTypes = {
  ADD_HISTORY: 'ADD_HISTORY'
};

/**
 * Perserve browsing history to local storage
 * @param {object} station - station object to be added to history store
 */
export const addHistory = station => ({
  type: historyActionTypes.ADD_HISTORY,
  payload: {
    station
  }
});