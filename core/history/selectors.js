import { HISTORY_STORAGE_KEY } from "../constants";

export const getHistory = state => {
  let historyJson = {};
  
  historyJson = state.localStorage.data && state.localStorage.data[HISTORY_STORAGE_KEY];
  if (!historyJson) return {};

  try {
    return JSON.parse(historyJson);
  } catch(e) {
    return historyJson;
  }
}