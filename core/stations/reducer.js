/**
 * @module station
 */

import { stationListActionTypes } from '../stationLists/actions';
import { stationActionTypes as actionTypes } from './actions';
import slugify from '../../util/slugify';

const cdnPfx = '//d35ry4sf1xc0zi.cloudfront.net/images/logos';

/**
 * From api we get links param shape: 
 * ```typescript
 * {
 *    id: number;
 *    slug: number;
 * }
 * ```
 * that needs to be transformed so the application can use those for routing
 * @param {object} linksObj
 */
function buildLinks(linksObj) {
  const links = {
    prev: null,
    next: null
  }
  if (!linksObj) return links;

  if (linksObj.prev && linksObj.prev.id) {
    links.prev = `/radio/${linksObj.prev.id}/${linksObj.prev.slug}`;
  }
  if (linksObj.next && linksObj.next.id) {
    links.next = `/radio/${linksObj.next.id}/${linksObj.next.slug}`;
  }

  return links;
}

/**
 * Here Station is normalized/tranformed so it can be used accross components
 * Single application interface
 * ```typescript
 * {
 *    id: number;
 *    title: string;
 *    slug: string;
 *    artworkUrl: string;
 *    stream: object;
 *    streamUrl: string;
 *    links: {
 *      prev: string;
 *      next: string;
 *    };
 *    genres: array<{
 *      name: string;
 *      id: number;
 *      slug: string;
 *    }>;
 *    city: string;
 *    country: string;
 *    nationality: string;
 *    typeName: string;
 *    typeF: string;
 *    networks: array<any>;
 *    loading: boolean;
 *    error: string;
 * }
 * ```
 * @param {object} data - response data
 * @param {boolean} loading 
 * @param {string} error - response error reason
 */
export function createStation(data, loading = false, error = null) {
  const title = data.name || '';
  const artworkUrl = data.artworkUrl || (
    data.logo ? `${cdnPfx}/${data.logo}` : ''
  );
  const streamUrl = data.streamUrl || (
    data.stream && data.stream.length ? (data.stream[0].stream_type === 1 && (RegExp(/(\.mp3)|(\/stream)/g).test(data.stream[0].stream_url)) ? data.stream[0].stream_url : `${data.stream[0].stream_url}/;*.mp3`) : ''
  );

  // const streamUrl = data.stream && data.stream.length && data.stream[0] && data.stream[0].stream_url;

  const genres = data.genres || [];
  const city = data.location && data.location.city || '';
  const country = data.country && data.country.name || '';
  const slug = data.slug || slugify(title);

  const homepage = data.homepage || '';

  const nationality =  data.nationality && data.nationality.lang && data.nationality.lang.length ? data.nationality.lang[0].url_name : process.env.DEFAULT_NATIONALITY;

  const links = buildLinks(data.links);
  
  return {
    id: data.id,
    title,
    slug,
    artworkUrl,
    stream: data.stream,
    streamUrl,
    links,
    
    genres,
    city, country, nationality,
    homepage,

    typeName: data.type_name,
    typeF: data.type_frequency,
    networks: data.networks,

    loading,
    error
  };
}

export const stationsExampleInitialState = {};

export default function stationsReducer(state = stationsExampleInitialState, { payload, type }) {

  switch(type) {

    case stationListActionTypes.FETCH_STATIONS_SUCCESS:
      const newStations = payload.collection.reduce((acc, n) => {
        acc[n.id] = createStation(n);
        return acc;
      }, {});
      return {
        ...state,
        ...newStations
      };



    case actionTypes.FETCH_STATION:
      return {
        ...state,
        [payload.stationId]: createStation({id: payload.stationId}, true)
      };

    case actionTypes.FETCH_STATION_SUCCESS:
    case actionTypes.LOAD_STATION:
      return {
        ...state,
        [payload.stationId]: createStation(payload.data)
      }

    case actionTypes.FETCH_STATION_FAILURE:
      return {
        ...state,
        [payload.stationId]: createStation({id: payload.stationId}, false, payload.error)
      };

    case actionTypes.VISIT_STATION_SUCCESS:
      const stationData = Object.assign({}, state[payload.stationId], payload.data);
      return {
        ...state,
        [payload.stationId]: stationData
      };

    default: 
      return state;
  }
}