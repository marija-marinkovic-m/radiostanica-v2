export function getStations(state) {
  return state.stations;
}

export function getStationById(stationId, state) {
  const stations = getStations(state);
  return stations && stations[stationId] ? stations[stationId] : false;
}