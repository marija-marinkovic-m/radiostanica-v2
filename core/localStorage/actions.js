/**
 * This module uses localStorage web [API](https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage)
 * @module localStorage
 */

export const localStorageActionTypes = {
  SET_LOCAL_DATA: 'SET_LOCAL_DATA',
  GET_LOCAL_DATA: 'GET_LOCAL_DATA',
  LOCAL_SET_SUCCESS: 'LOCAL_SET_SUCCESS',
  LOCAL_GET_SUCCESS: 'LOCAL_GET_SUCCESS',
  LOCAL_SET_FAILURE: 'LOCAL_SET_FAILURE',
  LOCAL_GET_FAILURE: 'LOCAL_GET_FAILURE',
  LOCAL_STORAGE_INIT: 'LOCAL_STORAGE_INITIALIZED'
}

export const failSet = (error) => ({
  type: localStorageActionTypes.LOCAL_SET_FAILURE,
  error
});
export const failGet = (error) => ({
  type: localStorageActionTypes.LOCAL_GET_FAILURE,
  error
});

export const successSet = (data) => ({
  type: localStorageActionTypes.LOCAL_SET_SUCCESS,
  data
});
export const successGet = (data) => ({
  type: localStorageActionTypes.LOCAL_GET_SUCCESS,
  data
});

/**
 * Get local storage data by key
 * @param {object} payload - object of shape {prop: string}, where prop is the data key 
 */
export const getLocalData = (payload) => ({
  type: localStorageActionTypes.GET_LOCAL_DATA, payload
});
/**
 * Store data action
 * @param {object} payload - object to be saved to storage, shape {key: string; value: string;}
 */
export const setLocalData = (payload) => ({
  type: localStorageActionTypes.SET_LOCAL_DATA, payload
});
/**
 * This action is used by the store to indicate that the local storage has been initialized
 */
export const storageInitialized = () => ({
  type: localStorageActionTypes.LOCAL_STORAGE_INIT
});