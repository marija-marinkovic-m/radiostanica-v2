/**
 * For translations we're using [i18next](https://www.i18next.com/) with [react-i18next](https://react.i18next.com/) packages  
 * 
 * Translations are loaded during initial render (in /pages/_app.js) and initialized.
 * 
 * All translations are stored in /static/locales/{language}/*.json
 * @module translations
 */

import { translationsActionTypes as actionTypes } from './actions';

/**
 * Translations entity store structure
 * ```typescript
 * {
 *    translations: object; // all loaded translations
 *    error: string; // reason
 *    loading: boolean;
 * }
 * ```
 */
export const translationsExampleInitialState = {
  error: null,
  loading: false,
  translations: null
}

function reducer(state = translationsExampleInitialState, action) {
  switch (action.type) {

    case actionTypes.SET_TRANSLATION:
      return {
        ...state,
        error: null,
        loading: true 
      }

    case actionTypes.SET_TRANSLATION_SUCCESS:
      return {
        ...state,
        loading: false,
        translations: { ...state.translations, ...action.data }
      }

    case actionTypes.SET_TRANSLATION_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error
      }

    default:
      return state;

  }
}

export default reducer;