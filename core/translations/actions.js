export const translationsActionTypes = {
  SET_TRANSLATION: 'SET_TRANSLATION',
  SET_TRANSLATION_SUCCESS: 'SET_TRANSLATION_SUCCESS',
  SET_TRANSLATION_FAILURE: 'SET_TRANSLATION_FAILURE'
};

export const setTranslation = (lang, namespaces = ['common', 'station']) => ({
  type: translationsActionTypes.SET_TRANSLATION,
  lang, namespaces
});

export const translationsSuccess = (data) => ({
  type: translationsActionTypes.SET_TRANSLATION_SUCCESS,
  data
});

export const translationsFailure = (error) => ({
  type: translationsActionTypes.SET_TRANSLATION_FAILURE,
  error
});