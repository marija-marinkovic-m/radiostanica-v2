export function getStoreTranslations(state) {
  return state.translations && state.translations.translations;
}