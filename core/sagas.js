import { all } from 'redux-saga/effects';
import localSagas from './localStorage/sagas';
import cookieSagas from './cookies/sagas';
import translationSagas from './translations/sagas';
import stationListsSagas from './stationLists/sagas';
import playerSagas from './player/sagas';
import favoritesSagas from './favorites/sagas';
import historySagas from './history/sagas';
import apiSagas from './api/sagas';

function* rootSaga() {
  yield all([
    ...localSagas,
    ...cookieSagas,
    ...translationSagas,
    ...stationListsSagas,
    ...playerSagas,
    ...favoritesSagas,
    ...historySagas,
    ...apiSagas
  ]);
}

export default rootSaga;