/**
 * @module
 * @alias module: player
 */

import { SESSION_STATIONLIST_ID } from '../constants';
import { playerActionTypes as actionTypes } from './actions';

/**
 * Player entiry store structure
 * ```typescript
 * {
 *    isPlaying: boolean;
 *    isReady: boolean;
 *    stationId: string | null;
 *    stationListId: string | SESSION_STATIONLIST_ID;
 *    volume: number; // range 0-1
 *    initialized: boolean;
 *    alert: string;
 * }
 * ```
 */
export const playerExampleInitialState = {
  isPlaying: false,
  isReady: false,
  stationId: null,
  stationListId: SESSION_STATIONLIST_ID,
  volume: 1,
  initialized: false,
  alert: ''
};

export default function playerReducer(state = playerExampleInitialState, { payload, type }) {
  switch (type) {

    case actionTypes.AUDIO_INITIALIZED:
      return {
        ...state,
        initialized: true
      }

    case actionTypes.AUDIO_PAUSED:
      return {
        ...state,
        isPlaying: false
      };

    case actionTypes.AUDIO_PLAYING:
      return {
        ...state,
        isPlaying: true
      };

    case actionTypes.AUDIO_READY:
      return {
        ...state,
        isReady: true
      };

    case actionTypes.AUDIO_VOLUME_CHANGED:
      return {
        ...state,
        volume: payload.volume
      };

    case actionTypes.PLAY_SELECTED:
      return {
        ...state,
        isReady: false,
        stationId: payload.station.id,
        stationListId: payload.stationListId || state.stationListId,
        alert: ''
      };

    case actionTypes.AUDIO_ERROR:
      return {
        ...state,
        isReady: false,
        alert: payload.msg
      }

    default: 
      return state;
  }
}