/**
 * Player Module uses HTML5 audio API.  
 * 
 * The HTML `audio` element is used to embed sound content in documents.
 * It may contain one or more audio sources, represented using the src attribute or the `source` element: the browser will choose the most suitable one.
 * 
 * Here it is used as the destination for streamed media, using a [MediaStream](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream).
 * @module player
 */

export const playerActionTypes = {
  // descriptions
  AUDIO_INITIALIZED: 'AUDIO_INITIALIZED',
  AUDIO_PAUSED: 'AUDIO_PAUSED',
  AUDIO_PLAYING: 'AUDIO_PLAYING',
  AUDIO_READY: 'AUDIO_READY',
  AUDIO_TIME_UPDATED: 'AUDIO_TIME_UPDATED',
  AUDIO_VOLUME_CHANGED: 'AUDIO_VOLUME_CHANGED',

  AUDIO_ERROR: 'AUDIO_ERROR',

  // imperatives
  PLAY_SELECTED: 'PLAY_SELECTED',
  PAUSE_AUDIO: 'PAUSE_AUDIO',
  AUDIO_SET_VOLUME: 'AUDIO_SET_VOLUME'
};

/**
 * Dispatch to indicate that audio player is successfully initialized
 */
export const audioInitialized = () => ({
  type: playerActionTypes.AUDIO_INITIALIZED
});

export const audioPaused = () => ({
  type: playerActionTypes.AUDIO_PAUSED
});

export const audioPlaying = () => ({
  type: playerActionTypes.AUDIO_PLAYING
});

export const audioReady = () => ({
  type: playerActionTypes.AUDIO_READY
});

export const audioTimeUpdated = times => ({
  type: playerActionTypes.AUDIO_TIME_UPDATED,
  payload: times
});

export const audioVolumeChanged = volume => ({
  type: playerActionTypes.AUDIO_VOLUME_CHANGED,
  payload: {
    volume
  }
});

/**
 * Dispatch to play specific station (station object with valid `id` prop must be provided)
 * @param {object} station - station object from store
 * @param {string} stationListId - current station's list (optional)
 */
export const playSelected = (station, stationListId = null) => ({
  type: playerActionTypes.PLAY_SELECTED,
  payload: {
    station,
    stationListId
  }
});

/**
 * Dispatch to stop/pause streaming
 */
export const pauseAudio = () => ({
  type: playerActionTypes.PAUSE_AUDIO
});

/**
 * Dispatch to increase/decrease sound volume
 * @param {number} volume - volume value from 0 to 1
 */
export const setVolume = volume => ({
  type: playerActionTypes.AUDIO_SET_VOLUME,
  payload: {
    volume
  }
});

/**
 * Dispatch to log streaming error for admins
 * @param {string} msg - reason the error occured
 */
export const streamingError = (msg) => ({
  type: playerActionTypes.AUDIO_ERROR,
  payload: {
    msg
  }
});