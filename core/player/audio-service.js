import * as playerActions from './actions';
import { api } from '../api/api-service';

export function AudioPlayer() {
  this._audio = new Audio();
  this._emiter;
  return ({
    initialize: emit => {
      this._emiter = emit;
      this._audio.preload = "none";
      this._audio.autoplay = false;
    
      this._audio.addEventListener(
        'pause',
        () => this._emiter(playerActions.audioPaused())
      );
      this._audio.addEventListener(
        'abort',
        () => this._emiter(playerActions.audioPaused())
      );
      this._audio.addEventListener(
        'playing',
        () => this._emiter(playerActions.audioPlaying())
      );
      this._audio.addEventListener(
        'volumechange',
        () => this._emiter(playerActions.audioVolumeChanged(this._audio.volume))
      );
      this._audio.addEventListener(
        'loadeddata',
        () => this._emiter(playerActions.audioReady())
      );

      return () => {};
    },
    load: url => {
      this._audio.setAttribute('src', url);
    },
    pause: () => {
      this._audio.setAttribute('src', 'about:blank');
      this._audio.pause();
    },
    play: () => {
      let promise = this._audio.play();
      if (promise && promise.catch) promise.catch((e) => {
        const now = new Date();
        const fileName = `stream-error-log-${(now.getMonth() + 1)}-${now.getYear()}`;

        // should emit error ???
        // if (this._emiter) this._emiter(playerActions.streamingError(`${e.message} (${now.getTime()})`))

        // log error 
        var data = [
          {
            time: now.toLocaleString().replace(', ', '-'),
            station: this._audio.getAttribute('src'),
            message: e.message.replace(',', '_')
          }
        ];
        api.streamErrorLog(data, fileName);
      });
    },
    setVolume: volume => {
      try {
        const value = parseFloat(volume);
        if (typeof value !== "number") return;
        this._audio.volume = value;
      } catch(e) {
        // error
      }
    }
  });
}