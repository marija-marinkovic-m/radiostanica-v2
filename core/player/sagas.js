import { eventChannel } from 'redux-saga';
import { call, fork, put, select, take, takeLatest } from 'redux-saga/effects';

import { localStorageActionTypes, setLocalData } from '../localStorage/actions';

import { playerActionTypes as actionTypes, playSelected, audioInitialized } from './actions';
import { AudioPlayer } from './audio-service';
import { getPlayerStationListCursor, getPlayerPrefs, getPlayerStationId, getPlayerIsPlaying } from './selectors';
import { PLAYER_STORAGE_KEY } from '../constants';

import { visitStation, visitStationSuccess, fetchStationSuccess } from '../stations/actions';
import { addHistory } from '../history/actions';

import es6promise from 'es6-promise';
import 'isomorphic-unfetch';
import { getStationById } from '../stations/selectors';
import { api } from '../api/api-service';
es6promise.polyfill();

let _audio;

export function* playNextStation() {
  const cursor = yield select(getPlayerStationListCursor);
  if (cursor.nextStationId) {
    yield put(playSelected(cursor.nextStationId))
  }
}

export function* playSelectedStation({payload: {station}}) {
  if (!_audio) return;

  if (!station || !station.id) return;


  if (!station.streamUrl) {
    let stationFromState = yield select(getStationById.bind(null, station.id));

    if (!stationFromState || !stationFromState.streamUrl) {
      try {
        // fetch station from api
        const stationFromApi = yield call(api.fetchStation, station.id);

        // push response to the app state
        yield put(fetchStationSuccess(station.id, stationFromApi));

        // get from the app state (as normalized version)
        stationFromState = yield select(getStationById.bind(null, station.id));

      } catch(e) {
        console.log('failed to fetch the station', e);
      }
    }

    if (!stationFromState || !stationFromState.streamUrl) return;

    station.streamUrl = stationFromState.streamUrl;
  }

  yield call(_audio.load, station.streamUrl);
  yield call(_audio.play);

  // count/visit
  yield put(visitStation(station.id));

  // notice history
  yield put(addHistory(station));
}

export function* pauseAudioStation() {
  if (!_audio) return;
  yield call(_audio.pause);
}

export function* subscribeToAudio() {
  _audio = new AudioPlayer();
  const channel = yield call(eventChannel, _audio.initialize);

  yield put(audioInitialized());

  // set volume from storage
  yield fork(setVolumeFromStorage);

  while(true) {
    let action = yield take(channel);
    yield put(action);
  }
}

export function* setVolumeFromStorage() {
  if (!_audio) return;
  const playerPrefs = yield select(getPlayerPrefs);
  yield call(_audio.setVolume, playerPrefs.volume);
}

export function* updateAudioVolume(action) {
  if (!_audio) return;
  const { payload:{volume} } = action;
  const newVolume = volume > 1 ? 1 : volume < 0 ? 0 : volume;
  yield call(_audio.setVolume, newVolume);
}

export function* saveVolumeToStorage(action) {
  const playerPrefs = yield select(getPlayerPrefs);
  yield put(setLocalData({
    key: PLAYER_STORAGE_KEY,
    value: {
      ...playerPrefs,
      volume: action.payload.volume
    }
  }));
}

// watchers
export function* watchInitApp() {
  while(true) {
    yield take(localStorageActionTypes.LOCAL_STORAGE_INIT);
    yield fork(subscribeToAudio);
  }
}

export function* watchPauseAudio() {
  while(true) {
    yield take(actionTypes.PAUSE_AUDIO);
    yield fork(pauseAudioStation);
  }
}

// root
export default [
  fork(watchInitApp),
  fork(watchPauseAudio),
  takeLatest(actionTypes.PLAY_SELECTED, playSelectedStation),
  takeLatest(actionTypes.AUDIO_SET_VOLUME, updateAudioVolume),
  takeLatest(actionTypes.AUDIO_VOLUME_CHANGED, saveVolumeToStorage)
];