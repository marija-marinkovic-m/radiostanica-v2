export const actionTypes = {
  MEDIA_CHANGE: '@mediaqueries/MEDIA_CHANGE'
}

export const mediaUpdate = () => ({
  type: actionTypes.MEDIA_CHANGE
});