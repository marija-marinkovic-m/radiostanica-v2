/**
 * This module is used for tracking document's width and height (usually on `window.resize` event).
 * @module mediaQueryTracker
 */

import { actionTypes } from './actions';

/**
 * Media entity store structure
 * ```typescript
 * {
 *    screenWidth: number;
 *    screenHeight: number;
 * }
 * ```
 */
export const mediaTrackerExampleInitialState = {
  screenWidth: 0,
  screenHeight: 0
};

/**
 * Trigger media update (typically on window.resize event)
 * @param {object} state - shape {screenWidth: number; screeenHeight: number;}
 * @param {object} action - shape {type: string}
 */
export default function mediaTrackerReducer (state = mediaTrackerExampleInitialState, action) {
  switch(action.type) {
    case actionTypes.MEDIA_CHANGE:
      if (typeof window === 'undefined' || typeof document === 'undefined') return state;
      let w = window,
          d = document,
          documentElement = d.documentElement,
          body = d.getElementsByTagName('body')[0],
          screenWidth = w.innerWidth || documentElement.clientWidth || body.clientWidth,
          screenHeight = w.innerHeight || documentElement.clientHeight || body.clientHeight;

      return Object.assign({}, state, {screenWidth, screenHeight})
    default: 
      return state;
  }
}