export const getScreenWidth = state => {
  return state.mediaQuery.screenWidth;
}

export const getScreenHeight = state => {
  return state.mediaQuery.screenHeight;
}