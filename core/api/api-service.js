/**
 * @module
 * @alias module:api
 */

import qs from 'query-string';
import { STATIONS_PER_PAGE } from '../constants';

import {
  normalizeNationality,
  normalizeCity
} from '../normalizers';

import es6promise from 'es6-promise';
import 'isomorphic-unfetch';
es6promise.polyfill();

const defaultStationListParams = {
  perPage: STATIONS_PER_PAGE,
  include: 'stream,info,details,nationality.lang,links,network',
  active: 'active_station',
  page: 1
};

const defaultStationParams = {
  include: 'stream,info,details,nationality.lang,links,network,links:active(active_station)'
};

// const makeRequest = (
//   requestUrl,
//   params = {method: 'GET', body: null}
// ) => {    
//   return fetch(requestUrl, params)
//     .then(res => res.json())
//     .catch(err => console.log('makeRequestErr', err));
// }

/**
 * Make Request to node proxy
 * @param {strign} requestUrl
 * @param {object} params request params including method, body, headers etc.
 */
export const makeRequest = (
  requestUrl,
  params = {method: 'GET', body: null, headers: { 'Content-Type': 'application/json' }}
) => {
  const origin = 
    typeof window !== "undefined" ?
    window.location.origin :
    `http://localhost:${process.env.PORT || 3000}`; // @temp
    
  return fetch(`${origin}/api`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      requestUrl,
      ...params

    })
  })
  .then(res => res.json())
  .catch(err => console.log('makeRequestErr', err));
}

export const api = {
  ping: () => {
    return makeRequest(requestUrl('/stations', {perPage: 1}));
  },
  fetchStations: (params = {}) => {
    const urlParams = Object.assign({}, defaultStationListParams, params);
    return makeRequest(requestUrl('/stations', urlParams))
      .then(response => ({
        collection: response && response.data,
        pagination: response && response.paginator,
        urlParams
      }));
  },
  fetchStation: (stationId, params = {}) => {
    const urlParams = Object.assign({}, defaultStationParams, params);
    return makeRequest(requestUrl(`/stations/${stationId}`, urlParams))
      .then(response => {
        if (response.status_code || response.message) throw Error(response);
        return response && response.data
      });
  },
  fetchResources: (resource, params) => {
    return makeRequest(requestUrl(`/${resource}`, params))
      .then(res => {
        const response = Object.assign({}, res);
        switch(resource) {
          case 'nationalities':
            response.data = res.data.map(n => normalizeNationality(n));
            break;
          case 'cities':
            response.data = res.data.map(c => normalizeCity(c));
            break;
        }
        return response;
      });
  },
  trackInfo: (stationId) => {
    return makeRequest(requestUrl(`/tracks-info/${stationId}`));
  },
  countVisit: (params) => {
    return makeRequest(requestUrl(`/count/visits`), params)
      .then(response => {
        return response;
      });
  },
  login: (username, password) => {
    return makeRequest(requestUrl('/auth/admin'), {
      method: 'POST',
      body: {
        username,
        password
      },
      headers: {
        'Content-Type': 'application/json'
      }
    });
  },
  streamErrorLog: (data, fileName = 'log') => {
    const origin =
      typeof window !== "undefined" ?
      window.location.origin :
      `http://localhost:${process.env.PORT || 3000}`; // @temp

    return fetch(`${origin}/api`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        streamLog: 1,
        fileName,
        data
      })
    })
    .then(res => res.json())
    .catch(err => console.log('makeRequestErr', err));
  },
  fetchStreamErrorLogList: () => {
    const origin =
      typeof window !== "undefined" ?
      window.location.origin :
      `http://localhost:${process.env.PORT || 3000}`; // @temp

    return fetch(`${origin}/api`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        fetchLogs: 1,
        path: './static/logs/'
      })
    })
    .then(res => res.json())
    .catch(err => console.log('makeRequestErr', err));
  }
};

function requestUrl (endpoint, query = null, apiUrl = process.env.API_URL) {
  const params = query ? '?' + qs.stringify(query) : '';
  return apiUrl + endpoint + params;
}