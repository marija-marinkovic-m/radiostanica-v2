/**
 * API module makes all the restapi requests (GET, PUT, POST, DELETE).
 * 
 * Main service uses isomorphic fetch for node and browserify (with es6-promise),
 * built on top of [GitHub's WHATWG Fetch polyfill](https://github.com/github/fetch)
 * @module api
 */

export const actions = {
  FETCH: '@apiRes/FETCH',
  FETCH_PENDING: '@apiRes/FETCH_PENDING',
  FETCH_SUCCESS: '@apiRes/FETCH_SUCCESS',
  FETCH_FAIL: '@apiRes/FETCH_FAIL',
  LOAD: '@apiRes/LOAD_FROM_STATE',

  GET_STATIONS: '@apiStations/GET_STATIONS',
  GET_STATION: '@apiStations/GET_STATION',
};

/**
 * Bulk get stations
 * @param {string} stationListId - station list id (required)
 * @param {object} params - http request params
 */
export const getStations = (stationListId, params) => ({
  type: actions.GET_STATIONS,
  payload: {
    stationListId,
    params
  }
});

/**
 * Fetch single station (from store or rest API)
 * @param {number} stationId - unique station id
 * @param {object} params - GET params
 */
export const getStation = (stationId, params = {}) => ({
  type: actions.GET_STATION,
  payload: {
    stationId,
    params
  }
});

/**
 * Indexes asked :resource
 * @param {string} resourceType - one of common resources (cities, countries, genres, promotions, types,...)
 * @param {object} params - GET request params
 */
export const fetchApiResource = (resourceType, params = {}) => ({
  type: actions.FETCH,
  payload: {
    resourceType,
    params
  }
});

export const fetchApiResourcePending = (resourceType) => ({
  type: actions.FETCH_PENDING,
  payload: {
    resourceType
  }
});

export const fetchApiResourceSuccess = (resourceType, response) => ({
  type: actions.FETCH_SUCCESS,
  payload: {
    resourceType,
    response
  }
});

export const fetchApiResourceFail = (error, resourceType) => ({
  type: actions.FETCH_FAIL,
  payload: {
    resourceType,
    error
  }
});

/**
 * Checks if resource exists in store or needs to be fetched from the api
 * used by sagas
 * @param {string} resourceType - one of common resources
 * @param {object} response - resource store object
 */
export const loadApiResource = (resourceType, response) => ({
  type: actions.LOAD,
  payload: {
    resourceType,
    response
  }
});