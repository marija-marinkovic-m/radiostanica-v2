/**
 * @module
 * @alias module:api
 */

import { actions as actionTypes } from './actions';

const resources = [
  'nationalities',
  'countries',
  'genres',
  'cities',
];

/**
 * Resource data from api store structure
 * Common resources: types, nationalities, countries, cities, promotions,...
 * ```typescript
 * [resource_name]: {
 *    loading: boolean;
 *    response: {
 *      data: []
 *    },
 *    error: string;
 *    isNew: boolean;
 * }
 * ```
 */
export const resourcesExampleInitialState = resources.reduce((acc, n) => {
  acc[n] = {loading: false, response: {data: []}, error: null, isNew: true};
  return acc;
}, {});

export function resourcesReducer(state = resourcesExampleInitialState, { type, payload = {resourceType: '', response: {data: []}, error: null} }) {
  const { resourceType, response, error } = payload;

  switch(type) {
    case actionTypes.FETCH_PENDING:
      return {
        ...state,
        [resourceType]: Object.assign({}, state[resourceType], {loading: true, error: null, isNew: false})
      };
    case actionTypes.FETCH_SUCCESS:
    case actionTypes.LOAD:
      return {
        ...state,
        [resourceType]: Object.assign({}, state[resourceType], {loading: false, response, error: null})
      };
    case actionTypes.FETCH_FAIL:
      return {
        ...state,
        [resourceType]: Object.assign({}, state[resourceType], {loading: false, error})
      };
    default:
      return state;
  }
}