/**
 * @module
 * @alias module:api
 */

import { call, put, takeEvery, select } from 'redux-saga/effects';
import { api } from './api-service';
import {
  actions,
  fetchApiResourceFail, fetchApiResourceSuccess, fetchApiResourcePending,
  loadApiResource
} from './actions';

import {
  fetchStationsFail, fetchStationsSuccess, fetchStationsPending,
  loadStationList
} from '../stationLists/actions';

import {
  fetchStationSuccess, fetchStationFail, fetchStationPending,
  loadStation
} from '../stations/actions';

import { getStationListById } from '../stationLists/selectors';
import { getStationById } from '../stations/selectors';
import { getResource } from './selectors';


export function* fetchStationsListSaga(action) {
  const { payload:{ stationListId, params } } = action;
  const stationList = yield select(getStationListById.bind(null, stationListId));
  if (stationList && stationList.currentPage >= params.page) {
    yield put(loadStationList(stationListId));
  } else {
    yield call(fetchStations, stationListId, params);
  }
}
export function* fetchStationSaga(action) {
  const { payload: {stationId, params} } = action;
  const station = yield select(getStationById.bind(null, stationId));
  if (station !== false) {
    yield put(loadStation(stationId, station));
  } else {
    yield call(fetchStation, stationId, stationId, params);
  }
}
function* fetchResourceSaga({type, payload: { resourceType, params }}) {
  const stateResource = yield select(getResource.bind(null, resourceType));
  if (stateResource !== false) {
    yield put(loadApiResource(resourceType, stateResource));
  } else {
    yield call(fetchResources, resourceType, resourceType, params)
  }
}

/**
 * All of the api sagas upon fork try to load resource from redux store
 * if no resource found, saga will try to fetch the resource from the rest API
 * on success, data will be stored
 * @param {function} apiFn - api service function to be used
 * @param {object} actions - action object with following props: `pending`, `success`, `fail`
 * @param {number} id - entity identificator
 * @param {csv} apiFnArgs - parameters for the api service function
 */
function* fetchEntities(apiFn, actions, id, ...apiFnArgs) {
  try {
    yield put(actions.pending(id));
    const data = yield call(apiFn, ...apiFnArgs);
    yield put(actions.success(id, data));
  } catch (error) {
    yield put(actions.fail(error, id));
  }
}

export const fetchStations = fetchEntities.bind(null, api.fetchStations, {
  pending: fetchStationsPending,
  success: fetchStationsSuccess,
  fail: fetchStationsFail
});
export const fetchStation = fetchEntities.bind(null, api.fetchStation, {
  pending: fetchStationPending,
  success: fetchStationSuccess,
  fail: fetchStationFail
});
export const fetchResources = fetchEntities.bind(null, api.fetchResources, {
  pending: fetchApiResourcePending,
  success: fetchApiResourceSuccess,
  fail: fetchApiResourceFail
})

export default [
  takeEvery(actions.GET_STATIONS, fetchStationsListSaga),
  takeEvery(actions.GET_STATION, fetchStationSaga),
  takeEvery(actions.FETCH, fetchResourceSaga)
];