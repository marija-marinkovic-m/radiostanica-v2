export function getResource(state, resourceSymbol) {
  if (!state.apiResources) return false;
  const res = state.apiResources[resourceSymbol];
  return res && !res.loading && !res.isNew ? res.response : false;
}

export function getResourceItems(state, resourceSymbol) {
  if (!state.apiResources) return [];
  if (!state.apiResources[resourceSymbol]) return [];
  if (!state.apiResources[resourceSymbol].response) return [];

  return state.apiResources[resourceSymbol].response.data || [];
}

export function getResourcePagination(state, resourceSymbol) {
  if (!state.apiResources) return null;
  if (!state.apiResources[resourceSymbol]) return null;
  if (!state.apiResources[resourceSymbol].response) return null;

  return state.apiResources[resourceSymbol].response.paginator || null;
}

export function getResourceStatus(state, resourceSymbol) {
  if (!state.apiResources) return false;
  if (!state.apiResources[resourceSymbol]) return false;

  return state.apiResources[resourceSymbol].loading;
}
export function getResourceError(state, resourceSymbol) {
  if (!state.apiResources) return null;
  if (!state.apiResources[resourceSymbol]) return null;

  return state.apiResources[resourceSymbol].error;
}

export function isSetSidebarNav(state) {
  const sidebarNav = getSidebarNav(state);
  return sidebarNav.links && sidebarNav.links.main.length ? sidebarNav : false;
}
export function getSidebarNav(state) {
  return state.sidebarNav;
}