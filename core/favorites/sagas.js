import { select, put, takeLatest } from 'redux-saga/effects';

import { favoritesActionTypes as actionTypes } from './actions';
import { getFavorites } from './selectors';
import { setLocalData } from '../localStorage/actions';
import { FAVORITES_STORAGE_KEY, STORAGE_STATIONS_LIMIT } from '../constants';

export function* toggleFavoriteSaga({payload: {station}}) {
  const favorites = yield select(getFavorites);
  const keys = Object.keys(favorites).map(f => String(f));
  const exists = keys.indexOf(String(station.id)) > -1;
  const limit = !exists ? STORAGE_STATIONS_LIMIT - 1 : keys.length;

  const value = keys
    .slice(0, limit)
    .filter(id => !exists || String(id) !== String(station.id))
    .reduce((acc, n) => ({...acc, [n]: favorites[n]}), exists ? {} : {[station.id]: station});

  yield put(setLocalData({
    key: FAVORITES_STORAGE_KEY,
    value
  }));
}

export default [
  takeLatest(actionTypes.TOGGLE_FAVORITE, toggleFavoriteSaga)
];