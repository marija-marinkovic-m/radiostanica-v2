import { FAVORITES_STORAGE_KEY } from "../constants";

export const getFavorites = state => {
  let favsJson = {};
  
  favsJson = state.localStorage.data && state.localStorage.data[FAVORITES_STORAGE_KEY];
  if (!favsJson) return {};

  try {
    return JSON.parse(favsJson);
  } catch(e) {
    return favsJson;
  }
}