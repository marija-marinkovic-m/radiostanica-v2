/**
 * Favorites module is responsible for toggling stations' state via localStorage
 * @module favorites
 */

export const favoritesActionTypes = {
  TOGGLE_FAVORITE: 'TOGGLE_FAVORITE'
};

/**
 * Station Favorites action
 * @param {object} station - station to be saved to, or deleted from, favorites store
 */
export const toggleFavorite = station => ({
  type: favoritesActionTypes.TOGGLE_FAVORITE,
  payload: {
    station
  }
});