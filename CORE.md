## Modules

<dl>
<dt><a href="#module_api">api</a></dt>
<dd><p>API module makes all the restapi requests (GET, PUT, POST, DELETE).</p>
<p>Main service uses isomorphic fetch for node and browserify (with es6-promise),
built on top of <a href="https://github.com/github/fetch">GitHub&#39;s WHATWG Fetch polyfill</a></p>
</dd>
<dt><a href="#module_api/api-service">api/api-service</a></dt>
<dd></dd>
<dt><a href="#module_api/reducer">api/reducer</a></dt>
<dd></dd>
<dt><a href="#module_api/sagas">api/sagas</a></dt>
<dd></dd>
<dt><a href="#module_cookies">cookies</a></dt>
<dd><p>Cookies Module is used for storing and parsing cookies both on the client and on the server side.</p>
<p>Utility functions from <code>/util/cookies.js</code> redirect every cookie request to cookie service
based on the component&#39;s (initial state) context
to <code>/util/cookie-client.js</code> and <code>/util/cookie-server.js</code> respectively</p>
</dd>
<dt><a href="#module_cookies/reducer">cookies/reducer</a></dt>
<dd></dd>
<dt><a href="#module_cookies/sagas">cookies/sagas</a></dt>
<dd></dd>
<dt><a href="#module_favorites">favorites</a></dt>
<dd><p>Favorites module is responsible for toggling stations&#39; state via localStorage</p>
</dd>
<dt><a href="#module_history">history</a></dt>
<dd><p>Module History is responsible for storing (predefined) number of stations to localStorage.
This number is sent to the module as environment variable.  </p>
<p>Newest stations are <code>unshifted</code> to the begining of history array.
The oldest are poped out.</p>
</dd>
<dt><a href="#module_localStorage">localStorage</a></dt>
<dd><p>This module uses localStorage web <a href="https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage">API</a></p>
</dd>
<dt><a href="#module_localStorage/reducer">localStorage/reducer</a></dt>
<dd></dd>
<dt><a href="#module_mediaQueryTracker">mediaQueryTracker</a></dt>
<dd><p>This module is used for tracking document&#39;s width and height (usually on <code>window.resize</code> event).</p>
</dd>
<dt><a href="#module_player">player</a></dt>
<dd><p>Player Module uses HTML5 audio API.  </p>
<p>The HTML <code>audio</code> element is used to embed sound content in documents.
It may contain one or more audio sources, represented using the src attribute or the <code>source</code> element: the browser will choose the most suitable one.</p>
<p>Here it is used as the destination for streamed media, using a <a href="https://developer.mozilla.org/en-US/docs/Web/API/MediaStream">MediaStream</a>.</p>
</dd>
<dt><a href="#module_player/reducer">player/reducer</a></dt>
<dd></dd>
<dt><a href="#module_station">station</a></dt>
<dd></dd>
<dt><a href="#module_translations">translations</a></dt>
<dd><p>For translations we&#39;re using <a href="https://www.i18next.com/">i18next</a> with <a href="https://react.i18next.com/">react-i18next</a> packages  </p>
<p>Translations are loaded during initial render (in /pages/_app.js) and initialized.</p>
<p>All translations are stored in /static/locales/{language}/*.json</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#setLocalDataSaga">setLocalDataSaga(param0)</a></dt>
<dd><p>This saga checks if storage available to store <code>param0</code> data</p>
</dd>
<dt><a href="#getLocalDataSaga">getLocalDataSaga(param0)</a></dt>
<dd><p>Check if storage available to fetch param0.payload.prop data
This method also announces storage initializations for other listeners</p>
</dd>
<dt><a href="#subscribeToStorage">subscribeToStorage()</a></dt>
<dd><p>This saga is used to emit and listen for event from other tabs</p>
</dd>
</dl>

<a name="module_api"></a>

## api
API module makes all the restapi requests (GET, PUT, POST, DELETE).Main service uses isomorphic fetch for node and browserify (with es6-promise),built on top of [GitHub's WHATWG Fetch polyfill](https://github.com/github/fetch)


* [api](#module_api)
    * [.getStations](#module_api.getStations)
    * [.getStation](#module_api.getStation)
    * [.fetchApiResource](#module_api.fetchApiResource)
    * [.loadApiResource](#module_api.loadApiResource)

<a name="module_api.getStations"></a>

### api.getStations
Bulk get stations

**Kind**: static constant of [<code>api</code>](#module_api)  

| Param | Type | Description |
| --- | --- | --- |
| stationListId | <code>string</code> | station list id (required) |
| params | <code>object</code> | http request params |

<a name="module_api.getStation"></a>

### api.getStation
Fetch single station (from store or rest API)

**Kind**: static constant of [<code>api</code>](#module_api)  

| Param | Type | Description |
| --- | --- | --- |
| stationId | <code>number</code> | unique station id |
| params | <code>object</code> | GET params |

<a name="module_api.fetchApiResource"></a>

### api.fetchApiResource
Indexes asked :resource

**Kind**: static constant of [<code>api</code>](#module_api)  

| Param | Type | Description |
| --- | --- | --- |
| resourceType | <code>string</code> | one of common resources (cities, countries, genres, promotions, types,...) |
| params | <code>object</code> | GET request params |

<a name="module_api.loadApiResource"></a>

### api.loadApiResource
Checks if resource exists in store or needs to be fetched from the apiused by sagas

**Kind**: static constant of [<code>api</code>](#module_api)  

| Param | Type | Description |
| --- | --- | --- |
| resourceType | <code>string</code> | one of common resources |
| response | <code>object</code> | resource store object |

<a name="module_api/api-service"></a>

## api/api-service
<a name="module_api/api-service.makeRequest"></a>

### api/api-service.makeRequest
Make Request to node proxy

**Kind**: static constant of [<code>api/api-service</code>](#module_api/api-service)  

| Param | Type | Description |
| --- | --- | --- |
| requestUrl | <code>strign</code> |  |
| params | <code>object</code> | request params including method, body, headers etc. |

<a name="module_api/reducer"></a>

## api/reducer
<a name="module_api/reducer.resourcesExampleInitialState"></a>

### api/reducer.resourcesExampleInitialState
Resource data from api store structureCommon resources: types, nationalities, countries, cities, promotions,...```typescript[resource_name]: {   loading: boolean;   response: {     data: []   },   error: string;   isNew: boolean;}```

**Kind**: static constant of [<code>api/reducer</code>](#module_api/reducer)  
<a name="module_api/sagas"></a>

## api/sagas
<a name="module_api/sagas..fetchEntities"></a>

### api/sagas~fetchEntities(apiFn, actions, id, ...apiFnArgs)
All of the api sagas upon fork try to load resource from redux storeif no resource found, saga will try to fetch the resource from the rest APIon success, data will be stored

**Kind**: inner method of [<code>api/sagas</code>](#module_api/sagas)  

| Param | Type | Description |
| --- | --- | --- |
| apiFn | <code>function</code> | api service function to be used |
| actions | <code>object</code> | action object with following props: `pending`, `success`, `fail` |
| id | <code>number</code> | entity identificator |
| ...apiFnArgs | <code>csv</code> | parameters for the api service function |

<a name="module_cookies"></a>

## cookies
Cookies Module is used for storing and parsing cookies both on the client and on the server side.Utility functions from `/util/cookies.js` redirect every cookie request to cookie servicebased on the component's (initial state) contextto `/util/cookie-client.js` and `/util/cookie-server.js` respectively


* [cookies](#module_cookies)
    * [.setCookie](#module_cookies.setCookie)
    * [.getCookie](#module_cookies.getCookie)
    * [.cookieSuccess](#module_cookies.cookieSuccess)
    * [.cookieFailure](#module_cookies.cookieFailure)

<a name="module_cookies.setCookie"></a>

### cookies.setCookie
Set cookie action

**Kind**: static constant of [<code>cookies</code>](#module_cookies)  

| Param | Type | Description |
| --- | --- | --- |
| ctx | <code>object</code> | react component context (next) |
| key | <code>string</code> | cookie storage key |
| value | <code>string</code> | cookie storage value |
| days | <code>number</code> | expiration indicator |

<a name="module_cookies.getCookie"></a>

### cookies.getCookie
Get cookie action

**Kind**: static constant of [<code>cookies</code>](#module_cookies)  

| Param | Type | Description |
| --- | --- | --- |
| ctx | <code>object</code> | next react component context (from getInitialProps) |
| key | <code>string</code> | cookie name (string) |
| defValue | <code>string</code> | if cookie doesn't exist |

<a name="module_cookies.cookieSuccess"></a>

### cookies.cookieSuccess
Cookie success

**Kind**: static constant of [<code>cookies</code>](#module_cookies)  

| Param | Type | Description |
| --- | --- | --- |
| key | <code>string</code> | ie. 'theme' |
| value | <code>string</code> | ie. 'light', 'dark',... |

<a name="module_cookies.cookieFailure"></a>

### cookies.cookieFailure
Cookie failure

**Kind**: static constant of [<code>cookies</code>](#module_cookies)  

| Param | Type | Description |
| --- | --- | --- |
| error | <code>string</code> | reason for failure |

<a name="module_cookies/reducer"></a>

## cookies/reducer
<a name="module_cookies/reducer.cookiesExampleInitialState"></a>

### cookies/reducer.cookiesExampleInitialState
Cookies entity store structure```typescript{   error: object | null;   loading: boolean;   theme: 'light' | 'dark';   lang: string;}```

**Kind**: static constant of [<code>cookies/reducer</code>](#module_cookies/reducer)  
<a name="module_cookies/sagas"></a>

## cookies/sagas

* [cookies/sagas](#module_cookies/sagas)
    * [~setCookieSaga(data)](#module_cookies/sagas..setCookieSaga)
    * [~getCookieSaga(data)](#module_cookies/sagas..getCookieSaga)

<a name="module_cookies/sagas..setCookieSaga"></a>

### cookies/sagas~setCookieSaga(data)
This saga checks request context (server / client)to store cookie by key to cookie storage

**Kind**: inner method of [<code>cookies/sagas</code>](#module_cookies/sagas)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>object</code> | has properties `ctx`, `key`, `value`, `days` |

<a name="module_cookies/sagas..getCookieSaga"></a>

### cookies/sagas~getCookieSaga(data)
This saga checks request context (server / client)to fetch cookie by key from cookie storage

**Kind**: inner method of [<code>cookies/sagas</code>](#module_cookies/sagas)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>object</code> | has properties `ctx`, `key`, `defValue` |

<a name="module_favorites"></a>

## favorites
Favorites module is responsible for toggling stations' state via localStorage

<a name="module_favorites.toggleFavorite"></a>

### favorites.toggleFavorite
Station Favorites action

**Kind**: static constant of [<code>favorites</code>](#module_favorites)  

| Param | Type | Description |
| --- | --- | --- |
| station | <code>object</code> | station to be saved to, or deleted from, favorites store |

<a name="module_history"></a>

## history
Module History is responsible for storing (predefined) number of stations to localStorage.This number is sent to the module as environment variable.  Newest stations are `unshifted` to the begining of history array.The oldest are poped out.

<a name="module_history.addHistory"></a>

### history.addHistory
Perserve browsing history to local storage

**Kind**: static constant of [<code>history</code>](#module_history)  

| Param | Type | Description |
| --- | --- | --- |
| station | <code>object</code> | station object to be added to history store |

<a name="module_localStorage"></a>

## localStorage
This module uses localStorage web [API](https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage)


* [localStorage](#module_localStorage)
    * [.getLocalData](#module_localStorage.getLocalData)
    * [.setLocalData](#module_localStorage.setLocalData)
    * [.storageInitialized](#module_localStorage.storageInitialized)

<a name="module_localStorage.getLocalData"></a>

### localStorage.getLocalData
Get local storage data by key

**Kind**: static constant of [<code>localStorage</code>](#module_localStorage)  

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>object</code> | object of shape {prop: string}, where prop is the data key |

<a name="module_localStorage.setLocalData"></a>

### localStorage.setLocalData
Store data action

**Kind**: static constant of [<code>localStorage</code>](#module_localStorage)  

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>object</code> | object to be saved to storage, shape {key: string; value: string;} |

<a name="module_localStorage.storageInitialized"></a>

### localStorage.storageInitialized
This action is used by the store to indicate that the local storage has been initialized

**Kind**: static constant of [<code>localStorage</code>](#module_localStorage)  
<a name="module_localStorage/reducer"></a>

## localStorage/reducer
<a name="module_localStorage/reducer.localStorageExampleInitialState"></a>

### localStorage/reducer.localStorageExampleInitialState
Local Storage entity store structure```typescript{   error: string | null;   loading: boolean;   data: object | null;}```

**Kind**: static constant of [<code>localStorage/reducer</code>](#module_localStorage/reducer)  
<a name="module_mediaQueryTracker"></a>

## mediaQueryTracker
This module is used for tracking document's width and height (usually on `window.resize` event).


* [mediaQueryTracker](#module_mediaQueryTracker)
    * [module.exports(state, action)](#exp_module_mediaQueryTracker--module.exports) ⏏
        * [.mediaTrackerExampleInitialState](#module_mediaQueryTracker--module.exports.mediaTrackerExampleInitialState)

<a name="exp_module_mediaQueryTracker--module.exports"></a>

### module.exports(state, action) ⏏
Trigger media update (typically on window.resize event)

**Kind**: Exported function  

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | shape {screenWidth: number; screeenHeight: number;} |
| action | <code>object</code> | shape {type: string} |

<a name="module_mediaQueryTracker--module.exports.mediaTrackerExampleInitialState"></a>

#### module.exports.mediaTrackerExampleInitialState
Media entity store structure```typescript{   screenWidth: number;   screenHeight: number;}```

**Kind**: static constant of [<code>module.exports</code>](#exp_module_mediaQueryTracker--module.exports)  
<a name="module_player"></a>

## player
Player Module uses HTML5 audio API.  The HTML `audio` element is used to embed sound content in documents.It may contain one or more audio sources, represented using the src attribute or the `source` element: the browser will choose the most suitable one.Here it is used as the destination for streamed media, using a [MediaStream](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream).


* [player](#module_player)
    * [.audioInitialized](#module_player.audioInitialized)
    * [.playSelected](#module_player.playSelected)
    * [.pauseAudio](#module_player.pauseAudio)
    * [.setVolume](#module_player.setVolume)
    * [.streamingError](#module_player.streamingError)

<a name="module_player.audioInitialized"></a>

### player.audioInitialized
Dispatch to indicate that audio player is successfully initialized

**Kind**: static constant of [<code>player</code>](#module_player)  
<a name="module_player.playSelected"></a>

### player.playSelected
Dispatch to play specific station (station object with valid `id` prop must be provided)

**Kind**: static constant of [<code>player</code>](#module_player)  

| Param | Type | Description |
| --- | --- | --- |
| station | <code>object</code> | station object from store |
| stationListId | <code>string</code> | current station's list (optional) |

<a name="module_player.pauseAudio"></a>

### player.pauseAudio
Dispatch to stop/pause streaming

**Kind**: static constant of [<code>player</code>](#module_player)  
<a name="module_player.setVolume"></a>

### player.setVolume
Dispatch to increase/decrease sound volume

**Kind**: static constant of [<code>player</code>](#module_player)  

| Param | Type | Description |
| --- | --- | --- |
| volume | <code>number</code> | volume value from 0 to 1 |

<a name="module_player.streamingError"></a>

### player.streamingError
Dispatch to log streaming error for admins

**Kind**: static constant of [<code>player</code>](#module_player)  

| Param | Type | Description |
| --- | --- | --- |
| msg | <code>string</code> | reason the error occured |

<a name="module_player/reducer"></a>

## player/reducer
<a name="module_player/reducer.playerExampleInitialState"></a>

### player/reducer.playerExampleInitialState
Player entiry store structure```typescript{   isPlaying: boolean;   isReady: boolean;   stationId: string | null;   stationListId: string | SESSION_STATIONLIST_ID;   volume: number; // range 0-1   initialized: boolean;   alert: string;}```

**Kind**: static constant of [<code>player/reducer</code>](#module_player/reducer)  
<a name="module_station"></a>

## station

* [station](#module_station)
    * _static_
        * [.createStation(data, loading, error)](#module_station.createStation)
    * _inner_
        * [~buildLinks(linksObj)](#module_station..buildLinks)

<a name="module_station.createStation"></a>

### station.createStation(data, loading, error)
Here Station is normalized/tranformed so it can be used accross componentsSingle application interface```typescript{   id: number;   title: string;   slug: string;   artworkUrl: string;   stream: object;   streamUrl: string;   links: {     prev: string;     next: string;   };   genres: array<{     name: string;     id: number;     slug: string;   }>;   city: string;   country: string;   nationality: string;   typeName: string;   typeF: string;   networks: array<any>;   loading: boolean;   error: string;}```

**Kind**: static method of [<code>station</code>](#module_station)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>object</code> | response data |
| loading | <code>boolean</code> |  |
| error | <code>string</code> | response error reason |

<a name="module_station..buildLinks"></a>

### station~buildLinks(linksObj)
From api we get links param shape: ```typescript{   id: number;   slug: number;}```that needs to be transformed so the application can use those for routing

**Kind**: inner method of [<code>station</code>](#module_station)  

| Param | Type |
| --- | --- |
| linksObj | <code>object</code> | 

<a name="module_translations"></a>

## translations
For translations we're using [i18next](https://www.i18next.com/) with [react-i18next](https://react.i18next.com/) packages  Translations are loaded during initial render (in /pages/_app.js) and initialized.All translations are stored in /static/locales/{language}/*.json

<a name="module_translations.translationsExampleInitialState"></a>

### translations.translationsExampleInitialState
Translations entity store structure```typescript{   translations: object; // all loaded translations   error: string; // reason   loading: boolean;}```

**Kind**: static constant of [<code>translations</code>](#module_translations)  
<a name="setLocalDataSaga"></a>

## setLocalDataSaga(param0)
This saga checks if storage available to store `param0` data

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| param0 | <code>object</code> | here we have `payload` property with expected value shape {key: string; value: string;} |

<a name="getLocalDataSaga"></a>

## getLocalDataSaga(param0)
Check if storage available to fetch param0.payload.prop dataThis method also announces storage initializations for other listeners

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| param0 | <code>object</code> | here we have `payload` property that contains `prop` value |

<a name="subscribeToStorage"></a>

## subscribeToStorage()
This saga is used to emit and listen for event from other tabs

**Kind**: global function  
