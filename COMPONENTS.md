Components
----------

**components\AdSense.js**

### 1. AdSense

Google AdSense component 

This component renders the Google AdSense code for showing AdSense banners.
```javascript
// ads with no set-up
<AdSense
  client='ca-pub-7292810486004926'
  slot='7806394673'
/>

// ads with custom format
<AdSense
  client='ca-pub-7292810486004926'
  slot='7806394673'
  style={{ width: 500, height: 300, float: 'left' }}
  format=''
/>

// responsive and native ads
<AdSense
  client='ca-pub-7292810486004926'
  slot='7806394673'
  style={{ display: 'block' }}
  layout='in-article'
  format='fluid'
/>
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
className|string|no|&lt;See the source code&gt;|React.Component &#x60;className&#x60; prop
style|object|no|&lt;See the source code&gt;|React.Component &#x60;style&#x60; prop
client|string|yes||AdSense client ID
slot|string|yes||AdSense slot ID
layout|string|no|&lt;See the source code&gt;|AdSense layout enum.
format|enum|no|&lt;See the source code&gt;|AdSense format enum
test|bool|no|false|Should indicate testing
-----
**components\ErrorComponent.js**

### 1. ErrorComponent

This component is used for when we don't have specific reason for request failure
```javascript
// usage
<ErrorComponent />
```   




-----
**components\LangSwitcher.js**

### 1. LangSwitcher

This component enables user to switch between available tranlations. 

It dispatches `setCookie` appstore action on demand, thus updating selected language globaly.
```javascript
// usage
<LangSwitcher />
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
lang|string|no||Selected language identifier
setCookie|func|no||Language switcher function
-----
**components\Pagination\Pagination.js**

### 1. Pagination

Component to render pagination
```javascript
// usage
<Pagination
   pageCount={12}
   pageRangeDisplayed={5}
   marginPagesDisplayed={1}
   initialPage={0}
   onPageChange={this.onPageChange}
/>
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
pageCount|number|no|10|The total number of pages.
pageRangeDisplayed|number|no|2|The range of pages displayed.
marginPagesDisplayed|number|no|3|The number of pages to display for margins
previousLabel|node|no|&lt;See the source code&gt;|Label for the &#x60;previous&#x60; button.
nextLabel|node|no|&lt;See the source code&gt;|Label for the &#x60;next&#x60; button.
breakLabel|node|no|&lt;See the source code&gt;|Label for ellipsis.
hrefBuilder|func|no||The method is called to generate the &#x60;href&#x60; attribute value on tag &#x60;a&#x60; of each page element.
onPageChange|func|no||The method to call when a page is clicked. Exposes the current page object as an argument.
initialPage|number|no||The initial page selected.
forcePage|number|no||To override selected page with parent prop.
disableInitialCallback|bool|no|false|Disable &#x60;onPageChange&#x60; callback with initial page
classes|object|yes||Classes to apply to pagination wrapper
t|func|yes||Translation method
-----
**components\Player\Player.js**

### 1. Player

Functional, statefull Player Component.

Used to render selected station info. Update player preferences and dispatch audio streaming.
```javascript
<Player query={q} />
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
player|object|no||Player object form redux store, shape &#x60;{isPlaying: boolean; isReady: boolean; stationId: number; stationListId: string; volume: number; initialized: boolean; alert: string}&#x60;
stations|object|no||Stations object from redux store
setVolume|func|no||Method to update audio volume
play|func|no||Method to dispath play selected station action
-----
**components\StationActions\FavStationToggler.js**

### 1. FavoriteToggler

Update the station's fav state based on the local storage data
```javascript
 <FavoriteToggler
     station={{
       id: number;
       ...
     }}
/>
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
station|object|yes||Station object
favorites|object|no||List of all favorites from store
t|func|no||Method for translations
-----
**components\StationActions\OptionsPopover.js**

### 1. OptionsPopover

Options component picks up the current station data to render available options inside the popover
```javascript
<OptionsPopover
   reportLink="mailto:office@mail.com" editable={true} />
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
reportLink|string|no||Link userd by report handler method
editable|bool|no||Does current user has editing permissions
-----
**components\StationActions\ShareStationButton.js**

### 1. ShareButton




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
t|func|yes||
-----
**components\StationActions\WebsiteStationButton.js**

### 1. WebsiteButton




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
t|func|yes||
classes|object|yes||
href|string|yes||
target|string|no|&lt;See the source code&gt;|
-----
**components\StationGrid\Card.js**

### 1. Station




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
classes|object|yes||
data|object|yes||
-----
**components\StationGrid\Grid.js**

### 1. StationsGrid




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
items|array|yes||
size|enum|no|&lt;See the source code&gt;|
animate|bool|no|true|
title|string|no||
link|string|no||
classes|object|yes||
t|func|yes||
-----
**components\StationList\Card.js**

### 1. StationCard




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
data|object|yes||
figureSize|shape|no|&lt;See the source code&gt;|
playButtonSize|enum|no|&lt;See the source code&gt;|
typeDisplay|bool|no|false|
classes|object|yes||
screenWidth|number|yes||
-----
**components\StationList\List.js**

### 1. StationList




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
items|array|no|&lt;See the source code&gt;|
animate|bool|no|true|
classes|object|yes||
-----
**components\StationListFilters.js**

### 1. Filters




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
parentParams|object|yes||
-----
**components\shared\ImgTag.js**

### 1. ImgTag




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
render|func|yes||
src|string|yes||
-----
**components\shared\PlayButton.js**

### 1. PlayButton




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
station|shape|yes||
forceplay|any|no|null|
size|enum|no|&lt;See the source code&gt;|
player|shape|yes||
play|func|yes||
pause|func|yes||
screenwidth|number|yes||
screenheight|number|yes||
-----
**components\shared\StationActivityBar.js**

### 1. StationActivityBar




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
t|func|yes||
classes|object|yes||
id|number|no|null|
mode|enum|no|&lt;See the source code&gt;|
alt||no|&lt;See the source code&gt;|
-----
**components\shared\StationMetaLinks.js**

### 1. StationMetaLinks




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
data|shape|yes||
typeDisplay|bool|no|false|
classes|object|yes||
-----

<sub>This document was generated by the <a href="https://github.com/marborkowski/react-doc-generator" target="_blank">**React DOC Generator v1.2.5**</a>.</sub>
