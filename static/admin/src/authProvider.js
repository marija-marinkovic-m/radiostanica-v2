import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK, fetchUtils } from 'react-admin';
import decodeJwt from 'jwt-decode';
import storage from './util/storage';
import { API_URL } from './constants';

export default (type, params) => {
  // when a user attempts to log in
  if (type === AUTH_LOGIN) {
    const { username, password } = params;
    const { fetchJson } = fetchUtils;

    return fetchJson(`${API_URL}/auth/admin`, {
        method: 'POST',
        body: JSON.stringify({username, password})
      })
      .then(response => {
        const { json } = response;
        if (json && json.data && json.data.token) {
          storage.save(json.data.token, decodeJwt(json.data.token));
          return Promise.resolve();
        }
        return Promise.reject('aor.auth.missing_token');
      })
      .catch(err => {
        if (err.body && typeof err.body === 'object') {
          if (err.body.hasOwnProperty('username')) return Promise.reject('aor.validation.username');
          if (err.body.hasOwnProperty('password')) return Promise.reject('aor.validation.password');
          if (err.body.hasOwnProperty('error') && typeof err.body.error === 'string') return Promise.reject(`aor.auth.${err.body.error}`);
        }
        return Promise.reject('aor.auth.sign_in_error');
      });
  }

  // when the user clicks on the logout button
  if (type === AUTH_LOGOUT) {
    storage.clear();
    return Promise.resolve();
  }

  // when the API returns an error
  if (type === AUTH_ERROR) {
    const { status } = params;
    if (status === 401 || status === 403) {
      storage.clear();
      return Promise.reject('aor.auth.unauthorized');
    }
    return Promise.resolve();
  }

  // when the user navigates to a new location
  if (type === AUTH_CHECK) {
    const token = storage.getItem('token');
    const exp = parseInt(storage.getItem('exp'), 10) * 1000;
    return token && exp && exp > Date.now()
      ? Promise.resolve()
      : Promise.reject();
  }

  return Promise.reject('Unknown method');
}