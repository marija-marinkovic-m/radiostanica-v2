import React from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import es6promise from 'es6-promise';
import 'isomorphic-unfetch';
es6promise.polyfill();

const logPath = './static/logs/';

export default class StreamLogs extends React.Component {
  state = {
    logs: []
  }
  componentDidMount() {
    // get list of all logs
    const origin =
    typeof window !== "undefined" ?
    window.location.origin :
    `http://localhost:${process.env.PORT || 3000}`; // @temp

    fetch(`${origin}/api`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        fetchLogs: 1,
        path: logPath
      })
    })
    .then(res => res.json())
    .then(files => {
      if (Array.isArray(files)) {
        this.setState({logs: files.filter(f => f !== '.gitkeep')})
      }
    })
    .catch(err => console.log('error Fetching log list', err));
  }
  render() {
    return <Paper>
      <Table>
        <TableHead><TableRow><TableCell>Stream Error Logs</TableCell></TableRow></TableHead>
        <TableBody>
          {this.state.logs.map((file,i) => {
            return <TableRow key={i}>
              <TableCell component="th" scope="row">
                <a href={`/static/logs/${file}`} download>{ file }</a>
              </TableCell>
            </TableRow>
          })}
        </TableBody>
      </Table>
      { !this.state.logs.length && (<p style={{
        margin: '15px 0',
        padding: '0 20px',
        fontFamily: 'Roboto, sans-serif'
      }}> No error logs available. </p>) }
    </Paper>
  }
}