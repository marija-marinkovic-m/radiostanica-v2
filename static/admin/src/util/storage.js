export default {
  save: (token, infoObj) => {
    localStorage.setItem('token', token);
    localStorage.setItem('exp', infoObj.exp);

    localStorage.setItem('decd', JSON.stringify(infoObj));
  },
  clear: () => {
    const apiFields = ['token', 'exp', 'decd'];
    apiFields.forEach(apiField => {
      localStorage.removeItem(apiField);
    });
  },
  getItem: (key) => localStorage.getItem(key),
  setItem: (prop, value) => localStorage.setItem(prop, value)
};