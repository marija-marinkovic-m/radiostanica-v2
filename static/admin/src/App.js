import React from 'react';
import {
  Admin,
  Resource
} from 'react-admin';

import authProvider from './authProvider';
import dataProvider from './dataProvider';

import {
  genreParams, promotionParams, stationParams,
  countriesParams, countriesAllParams, nationalitiesParams,
  citiesParams, typeParams, stationStreamParams,
  stationStreamInfoParams
} from './resources';

import customRoutes from './customRoutes';

const App = () => (
  <Admin
    authProvider={authProvider}
    dataProvider={dataProvider}
    customRoutes={customRoutes}
    title="Radiostanica.com">
    <Resource {...stationParams} />
    <Resource {...genreParams} />
    <Resource {...promotionParams} />
    <Resource {...countriesParams} />
    <Resource {...countriesAllParams} />
    <Resource {...nationalitiesParams} />
    <Resource {...citiesParams} />
    <Resource {...typeParams} />
    <Resource {...stationStreamParams} />
    <Resource {...stationStreamInfoParams} />
  </Admin>
);

export default App;