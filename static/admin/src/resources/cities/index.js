import React from 'react';
import {
  List, Datagrid, TextField, EditButton, ReferenceField,
  Edit, SimpleForm, DisabledInput, TextInput, ReferenceInput, AutocompleteInput,
  Create, Filter
} from 'react-admin';

import BooleanField from '../../fields/BooleanCustomized';
import BooleanInput from '../../inputs/BooleanInputCustom';
import CityIcon from '@material-ui/icons/LocationCity';

const ListFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="name" alwaysOn />
  </Filter>
);

const ResourceList = (props) => (
  <List {...props} filters={<ListFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <BooleanField source="active" />
      <ReferenceField label="Country" source="country_id" reference="countries-all">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField label="Nationality" source="nationality_id" reference="nationalities">
        <TextField source="iso_code" />
      </ReferenceField>
      <TextField source="lat" />
      <TextField source="lng" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <BooleanInput source="active" />
      <ReferenceInput label="Country" source="country_id" reference="countries-all" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput label="Nationality" source="nationality_id" reference="nationalities" filterToQuery={iso_code => ({iso_code})}>
        <AutocompleteInput optionText="iso_code" />
      </ReferenceInput>
      <TextInput source="lat" />
      <TextInput source="lng" />
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <BooleanInput source="active" />
      <ReferenceInput label="Country" source="country_id" reference="countries-all" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput label="Nationality" source="nationality_id" reference="nationalities" filterToQuery={iso_code => ({iso_code})}>
        <AutocompleteInput optionText="iso_code" />
      </ReferenceInput>
      <TextInput source="lat" />
      <TextInput source="lng" />
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>City: {record ? `"${record.name}"` : 'untitled'}</span>

export default {
  name: 'cities',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: CityIcon
};