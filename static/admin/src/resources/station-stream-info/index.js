import React from 'react';
import {
  List, Datagrid, TextField, ReferenceField, EditButton,
  Edit, SimpleForm, DisabledInput, TextInput, ReferenceInput,
  AutocompleteInput,
  Create
} from 'react-admin';
import Info from '@material-ui/icons/Info';
import { BooleanInput } from '../../inputs/BooleanInputCustom';

const ResourceList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="radiostation_id" />
      <ReferenceField label="Station" source="radiostation_id" reference="stations">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="stream_url" />
      <TextField source="stream_type" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <ReferenceInput label="Station" source="radiostation_id" reference="stations" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <TextInput source="stream_url" />
      <TextInput source="server_url" />
      <TextInput source="stream_type" />
      <BooleanInput source="main" />

      <TextInput source="streaming_id" />
      <TextInput source="stream_info_url" />
      <TextInput source="mount_point" />
      
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="Station" source="radiostation_id" reference="stations" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <TextInput source="stream_url" />
      <TextInput source="server_url" />
      <TextInput source="stream_type" />
      <BooleanInput source="main" />

      <TextInput source="streaming_id" />
      <TextInput source="stream_info_url" />
      <TextInput source="mount_point" />
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Stream URL {record ? `"${record.stream_url}"` : 'untitled'}</span>

export default {
  name: 'station-stream-info',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: Info
};