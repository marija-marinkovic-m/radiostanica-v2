import React from 'react';
import {
  List, Datagrid, TextField, ReferenceField, EditButton,
  Edit, SimpleForm, DisabledInput, TextInput, ReferenceInput,
  AutocompleteInput, SelectInput,
  Create
} from 'react-admin';
import GiftCard from '@material-ui/icons/CardGiftcard';

const activityChoices = [
  {id: 0, name: 'Status: 0'},
  {id: 1, name: 'Status: 1'},
  {id: 2, name: 'Status: 2'}
];

const ResourceList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="radiostation_id" />
      <ReferenceField label="Station" source="radiostation_id" reference="stations">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="active" />
      <TextField source="date_from" />
      <TextField source="date_to" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <ReferenceInput label="Station" source="radiostation_id" reference="stations" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <SelectInput source="active" choices={activityChoices} />
      <TextInput source="date_from" />
      <TextInput source="date_to" />
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="Station" source="radiostation_id" reference="stations" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <SelectInput source="active" choices={activityChoices} />
      <TextInput source="date_to" />
      <TextInput source="date_from" />
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Promotion until: {record ? `"${record.date_to}"` : 'untitled'}</span>

export default {
  name: 'promotions',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: GiftCard
};