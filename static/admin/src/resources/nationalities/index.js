import React from 'react';
import {
  List, Datagrid, TextField, EditButton, ReferenceField,
  Edit, SimpleForm, DisabledInput, TextInput, ReferenceInput, AutocompleteInput,
  Create
} from 'react-admin';

import BooleanField from '../../fields/BooleanCustomized';
import BooleanInput from '../../inputs/BooleanInputCustom';
import LanguageIcon from '@material-ui/icons/Language';

const ResourceList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="iso_code" />
      <BooleanField source="active" />
      <ReferenceField label="Country" source="country_id" reference="countries-all">
        <TextField source="name" />
      </ReferenceField>
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="iso_code" />
      <BooleanInput source="active" />
      <ReferenceInput label="Country" source="country_id" reference="countries-all" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="iso_code" />
      <BooleanInput source="active" />
      <ReferenceInput label="Country" source="country_id" reference="countries-all" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Nationality: {record ? `"${record.ico_code}"` : 'untitled'}</span>

export default {
  name: 'nationalities',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: LanguageIcon
};