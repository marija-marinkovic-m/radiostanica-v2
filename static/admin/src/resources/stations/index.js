import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import ErrorOutline from '@material-ui/icons/ErrorOutline';

import {
  List, Datagrid, TextField, EditButton, ChipField, UrlField, FunctionField,
  Edit, SimpleForm, DisabledInput, TextInput, SelectInput, ReferenceInput,
  AutocompleteInput, NumberInput, SelectArrayInput, ReferenceArrayInput, 
  Create,
  CardActions, CreateButton, RefreshButton, Filter
} from 'react-admin';

import RadioIcon from '@material-ui/icons/Radio';
import BooleanField from '../../fields/BooleanCustomized';
import { BooleanInput } from '../../inputs/BooleanInputCustom';

const radioStationStatus = [
  { id: 'new_station', name: 'New' },
  { id: 'active_station', name: 'Active' },
  { id: 'inactive_station', name: 'Inactive' },
  { id: 'deleted_station', name: 'Deleted' }
];

const streamServerType = [
  { id: 'shoutcast2_stream_server', name: 'shoutcast2_stream_server' },
  { id: 'icecast_stream_server', name: 'icecast_stream_server' },
  { id: 'other_stream_server', name: 'other_stream_server' }
];

const streamPlayer = [
  { id: 'jw_player', name: 'jw_player' },
  { id: 'jw_player_4', name: 'jw_player_4' },
  { id: 'muses_player', name: 'muses_player' },
  { id: 'audio_tag', name: 'audio_tag' }
];

const renderGenreChips = (record) => record.genres.map(g => <ChipField key={g.slug} record={g} source="name" />);

const ListActions = ({resource, filters, displayedFilters, filterValues, basePath, showFilter}) => (
  <CardActions>
    {filters && React.cloneElement(filters, {
      resource,
      showFilter,
      displayedFilters,
      filterValues,
      context: 'button',
    }) }
    <CreateButton basePath={basePath} />
    <RefreshButton />
    <Link to="/stream-logs">
      <Button>
        <ErrorOutline style={{marginRight: '7px'}} />
        Stream Logs
      </Button>
    </Link>
  </CardActions>
);

const ListFilters = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="name" alwaysOn />
  </Filter>
);

const ResourceList = (props) => (
  <List {...props} filters={<ListFilters />} actions={<ListActions />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="type_name" />
      <FunctionField label="Genres" render={renderGenreChips} />
      
      <BooleanField source="active" />
      <UrlField source="homepage" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <SelectInput source="active" choices={radioStationStatus} />
      <ReferenceInput label="City" source="city" reference="cities" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput label="Country" source="country_id" reference="countries-all" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput label="Nationality" source="nationality_id" reference="nationalities" filterToQuery={iso_code => ({iso_code})}>
        <AutocompleteInput optionText="iso_code" />
      </ReferenceInput>
      <TextInput source="stream" />
      <SelectInput source="stream_server_type" choices={streamServerType} />
      <TextInput source="track_info_url" />
      <TextInput source="homepage" />
      <ReferenceInput label="Type" source="type_id" reference="types" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <TextInput source="email" />
      <TextInput source="latitude" />
      <TextInput source="longitude" />

      {/* @TODO: GENRES */}
      <ReferenceArrayInput source="genres" reference="genres" allowEmpty>
        <SelectArrayInput optionText="name" />
      </ReferenceArrayInput>
      

      <TextInput source="stream_rtmp" />
      <TextInput source="stream_type" />
      <SelectInput source="stream_player" choices={streamPlayer} />
      <TextInput source="track_info_mount_point" />
      <BooleanInput source="track_info" />
      <NumberInput source="listeners_info" />
      <TextInput source="stream_player_link" />

      <TextInput source="stream_download_link" />
      <TextInput source="type_frequency" />
      <TextInput source="info" />
      <TextInput source="first_name" />
      <TextInput source="last_name" />
      <TextInput source="phone" />
      <TextInput source="mobile_phone" />
      <TextInput source="skype" />
      <TextInput source="facebook" />
      <TextInput source="comment" />
      <TextInput source="editor" />
     
     {/* @TODO: parent_id */}

      <TextInput source="zendesk_url" />
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <SelectInput source="active" choices={radioStationStatus} />
      <ReferenceInput label="City" source="city" reference="cities" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput label="Country" source="country_id" reference="countries-all" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput label="Nationality" source="nationality_id" reference="nationalities" filterToQuery={iso_code => ({iso_code})}>
        <AutocompleteInput optionText="iso_code" />
      </ReferenceInput>
      <TextInput source="stream" />
      <SelectInput source="stream_server_type" choices={streamServerType} />
      <TextInput source="track_info_url" />
      <TextInput source="homepage" />
      <ReferenceInput label="Type" source="type_id" reference="types" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <TextInput source="email" />
      <TextInput source="latitude" />
      <TextInput source="longitude" />

      {/* @TODO: GENRES */}

      <TextInput source="stream_rtmp" />
      <TextInput source="stream_type" />
      <SelectInput source="stream_player" choices={streamPlayer} />
      <TextInput source="track_info_mount_point" />
      <BooleanInput source="track_info" />
      <NumberInput source="listeners_info" />
      <TextInput source="stream_player_link" />

      <TextInput source="stream_download_link" />
      <TextInput source="frequency" />
      <TextInput source="info" />
      <TextInput source="first_name" />
      <TextInput source="last_name" />
      <TextInput source="phone" />
      <TextInput source="mobile_phone" />
      <TextInput source="skype" />
      <TextInput source="facebook" />
      <TextInput source="comment" />
      <TextInput source="editor" />
     
     {/* @TODO: parent_id */}

      <TextInput source="zendesk_url" />
      

    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Station: {record ? `"${record.name}"` : 'untitled'}</span>

export default {
  name: 'stations',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: RadioIcon
};