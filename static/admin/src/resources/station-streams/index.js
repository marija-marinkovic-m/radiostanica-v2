import React from 'react';
import {
  List, Datagrid, TextField, ReferenceField, EditButton,
  Edit, SimpleForm, DisabledInput, TextInput, ReferenceInput,
  AutocompleteInput, SelectInput,
  Create
} from 'react-admin';
import ViewStream from '@material-ui/icons/ViewStream';
import { BooleanInput } from '../../inputs/BooleanInputCustom';

const streamPlayer = [
  { id: 'jw_player', name: 'jw_player' },
  { id: 'jw_player_4', name: 'jw_player_4' },
  { id: 'muses_player', name: 'muses_player' },
  { id: 'audio_tag', name: 'audio_tag' }
];

const serverType = [
  { id: 1, name: 'Type 1' },
  { id: 2, name: 'Type 2' },
  { id: 3, name: 'Type 3' }
];

const ResourceList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="radiostation_id" />
      <ReferenceField label="Station" source="radiostation_id" reference="stations">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="stream_url" />
      <TextField source="server_url" />
      <TextField source="stream_type" />
      <TextField source="server_type" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <ReferenceInput label="Station" source="radiostation_id" reference="stations" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <SelectInput source="player" choices={streamPlayer} />
      <TextInput source="stream_url" />
      <TextInput source="server_url" />
      <TextInput source="stream_type" />
      <SelectInput source="server_type" choices={serverType} />
      <BooleanInput source="main" />

      <TextInput source="stream_rtmp" />
      <TextInput source="player_link" />
      <TextInput source="download_link" />
      
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="Station" source="radiostation_id" reference="stations" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <SelectInput source="player" choices={streamPlayer} />
      <TextInput source="stream_url" />
      <TextInput source="server_url" />
      <TextInput source="stream_type" />
      <SelectInput source="server_type" choices={serverType} />
      <BooleanInput source="main" />

      <TextInput source="stream_rtmp" />
      <TextInput source="player_link" />
      <TextInput source="download_link" />
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Stream URL {record ? `"${record.stream_url}"` : 'untitled'}</span>

export default {
  name: 'station-streams',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: ViewStream
};