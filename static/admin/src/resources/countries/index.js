import React from 'react';
import {
  List, Datagrid, TextField, EditButton,
  Edit, SimpleForm, DisabledInput, TextInput,
  Create
} from 'react-admin';

import BooleanField from '../../fields/BooleanCustomized';
import BooleanInput from '../../inputs/BooleanInputCustom';
import MyLocation from '@material-ui/icons/MyLocation';

const ResourceList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="iso_code" />
      <BooleanField source="active" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="iso_code" />
      <BooleanInput source="active" />
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="iso_code" />
      <BooleanInput source="active" />
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Country iso code: {record ? `"${record.ico_code}"` : 'untitled'}</span>

export default {
  name: 'countries',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: MyLocation
};