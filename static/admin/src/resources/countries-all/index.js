import React from 'react';
import {
  List, Datagrid, TextField, EditButton,
  Edit, SimpleForm, DisabledInput, TextInput,
  Create, Filter
} from 'react-admin';

import PublicIcon from '@material-ui/icons/Public';

const ListFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="name" alwaysOn />
  </Filter>
);

const ResourceList = (props) => (
  <List {...props} filters={<ListFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="iso2" />
      <TextField source="iso3" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <TextInput source="iso2" />
      <TextInput source="iso3" />
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="iso_code" />
      <TextInput source="iso2" />
      <TextInput source="iso3" />
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Country {record ? `"${record.name}"` : 'untitled'}</span>

export default {
  name: 'countries-all',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: PublicIcon
};