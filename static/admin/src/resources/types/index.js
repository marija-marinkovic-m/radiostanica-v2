import React from 'react';
import {
  List, Datagrid, TextField, EditButton,
  Edit, SimpleForm, DisabledInput, TextInput,
  Create
} from 'react-admin';

import TypeIcon from '@material-ui/icons/Style';

const ResourceList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Type: {record ? `"${record.name}"` : 'untitled'}</span>

export default {
  name: 'types',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: TypeIcon
};