import React from 'react';
import {
  List, Datagrid, TextField, EditButton,
  Edit, SimpleForm, DisabledInput, TextInput,
  Create, Filter
} from 'react-admin';

import TagIcon from '@material-ui/icons/LocalOffer';

const ListFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="name" alwaysOn />
  </Filter>
);

const ResourceList = (props) => (
  <List {...props} filters={<ListFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="slug" />
      <EditButton />
    </Datagrid>
  </List>
);

const ResourceEdit = (props) => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);

const ResourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);

const Title = ({record}) => <span>Genre {record ? `"${record.name}"` : 'untitled'}</span>

export default {
  name: 'genres',
  list: ResourceList,
  edit: ResourceEdit,
  create: ResourceCreate,
  icon: TagIcon
};