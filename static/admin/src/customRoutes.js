import React from 'react';
import { Route } from 'react-router-dom';
import StreamLogs from './components/StreamLogs';

export default [
  <Route exact path="/stream-logs" component={StreamLogs} />
];