import {
  GET_LIST,
  GET_ONE,
  UPDATE,
  CREATE,
  DELETE,
  GET_MANY,
  GET_MANY_REFERENCE,
  fetchUtils
} from 'react-admin';
import { stringify } from 'query-string';

import get from 'lodash.get';

import { API_URL } from './constants';
import storage from './util/storage';

/**
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */
const convertDataProviderRequestToHTTP = (type, resource, params) => {
    const options = {
      headers: new Headers({ Accept: 'application/json' })
    }
    const token = storage.getItem('token');
    if (token) {
      options.headers.set('Authorization', `Bearer ${token}`)
    }

  switch(type) {
    case GET_LIST: {
      const { page, perPage } = params.pagination;
      const { field, order } = params.sort;

      const getListQuery = stringify(Object.assign({}, {
        page, perPage, sort: `${field}:${order}`
      }, params.filter), {arrayFormat: 'bracket'});
      return { url: `${API_URL}/${resource}?${getListQuery}`, options };
    }
    case GET_ONE: {
      const singleUrl = `${API_URL}/${resource}/${params.id}`;
      if ('stations' === resource) return {
        url: `${singleUrl}?include=info,details,nationalty`,
        options
      };
      return { url: singleUrl, options };
    }
    case GET_MANY: {
      const filter = {id: []};
      params.ids.forEach(element => {
        if (isNaN(element)) return;
        filter.id.push(element);
      });
      const getManyQuery = stringify({ filter: JSON.stringify({ id: filter.id }) }, {arrayFormat: 'bracket'});
      return { url: `${API_URL}/${resource}?${getManyQuery}`, options };
    }
    case GET_MANY_REFERENCE: {
      const getManyReferenceQuery = stringify(Object.assign({}, {
        page: params.pagination.page,
        perPage: params.pagination.perPage,
        sort: `${params.sort.field}:${params.sort.order}`,
        [params.target]: params.id
      }, params.filter), {arrayFormat: 'bracket'});
      return { url: `${API_URL}/${resource}?${getManyReferenceQuery}`, options };
    }
    case UPDATE: {
      return {
        url: `${API_URL}/${resource}/${params.id}`,
        options: { method: 'PUT', body: JSON.stringify(params.data), ...options }
      };
    }
    case CREATE: {
      return {
        url: `${API_URL}/${resource}`,
        options: { method: 'POST', body: JSON.stringify(params.data), ...options }
      };
    }
    case DELETE: {
      return {
        url: `${API_URL}/${resource}/${params.id}`,
        options: { method: 'DELETE', ...options }
      };
    }
    default:
      throw new Error(`Unsupported fetch action type ${type}`);
  }
}

/**
 * @param {Object} response HTTP response from fetch()
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} Data Provider response
 */
const convertHTTPResponseToDataProvider = (response, type, resource, params) => {
  const { /* headers, */ json: {data, paginator} } = response;
  switch(type) {
    case GET_LIST:
    case GET_MANY_REFERENCE: {
      return {
        data,
        total: (paginator && paginator.total) || data.length
      };
    }
    case GET_ONE:
      // transform responses
      console.log('from transform responses', resource);
      if ('stations' === resource) {
        return {
          data: {
            id: data.id,
            active: data.active,
            homepage: data.homepage,
            name: data.name,
            slug: data.slug,
            type_frequency: data.type_frequency,
            country_id: get(data, 'country.id', 0),
            genres: [],
            ...get(data, 'details', {}),
            ...get(data, 'location', {})
          }
        }
      }
      return { data };
    case CREATE: {
      return {
        data: {
          ...params.data,
          id: response.json.id,
          json: response.json
        }
      };
    }
    default:
      return { data };
  }
}

/**
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for response
 */

 export default (type, resource, params) => {
   const { fetchJson } = fetchUtils;
   const { url, options } = convertDataProviderRequestToHTTP(type, resource, params);
   return fetchJson(url, options)
    .then(response => convertHTTPResponseToDataProvider(response, type, resource, params))
    .catch(error => ({json: {data: {name: 'error'}}}));
 }