import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

// import { Router } from '../routes';
import withRoot from '../core/withRoot';

import {
  getStations as getStationsAction,
  fetchApiResourceSuccess as fetchApiResourceSuccessAction
} from '../core/api/actions';

import { api } from '../core/api/api-service';

import { getStations as selectStations } from '../core/stations/selectors';
import { getStationLists as selectStationLists } from '../core/stationLists/selectors';

import { Grid as StationGrid } from '../components/StationGrid';
import { DEFAULT_NATIONALITY_KEY } from '../core/constants';
import { getCookie, setCookie } from '../core/cookies/actions';
import { getScreenWidth } from '../core/mediaQueryTracker/selectors';
// import AdSense from '../components/AdSense';

class FrontPage extends React.Component {
  static async getInitialProps({ctx: { query, store, isServer, req, res }}) {
    let listData = [];
    const defaultNationality = store.getState().cookies[DEFAULT_NATIONALITY_KEY];

    const getResource = (resource, state) => Boolean(
      resource in state.apiResources
      && state.apiResources.resource
      && 'response' in state.apiResources.resource
      && 'data' in state.apiResources.resource.response
    ) ? state.apiResources.resource.response.data : [];

    // 1. here we need all nationalities (and posibly check and store and isServer them for later)
    let nationalities = [];
    if (!isServer) {
      nationalities = getResource('nationalities', store.getState());
    }

    if (!nationalities.length) {
      nationalities = await api.fetchResources('nationalities', {include: 'lang'})
        .then(response => {
          store.dispatch(fetchApiResourceSuccessAction('nationalities', response));
          return response.data;
        })
        .catch(err => []);
    }

    const currNationalityLang = query.nationality || defaultNationality;
    const currNationality = nationalities.filter(n => n.lang === currNationalityLang);

    if (currNationalityLang && currNationalityLang !== defaultNationality) {
      store.dispatch(setCookie({req, res}, DEFAULT_NATIONALITY_KEY, currNationalityLang));
    }

    // 2. now, check for currNationalityLang
    if (currNationalityLang && currNationality.length) {
      // we need four fetches now, 1 by popularity, 1 by genre and 2 by the city
      // so first check for isServer, resources and store them for later on
      // we need /genres && /cities?include=nationality.lang
      let genres = [];
      let cities = [];
      if (!isServer) {
        genres = getResource('genres', store.getState());
        cities = getResource('cities', store.getState());
      }

      if (!genres.length) {
        genres = await api.fetchResources('genres')
          .then(response => {
            store.dispatch(fetchApiResourceSuccessAction('genres', response));
            return response.data;
          })
          .catch(err => []);
      }

      if (!cities.length) {
        cities = await api.fetchResources('cities', {include: 'nationality.lang'})
          .then(response => {
            store.dispatch(fetchApiResourceSuccessAction('cities', response));
            return response.data;
          })
          .catch(err => []);
      }

      // and then create an array of the listData according to genre/location/nationality
      // each entry will have `storeId`, `name` and `slug` properties + params helper
      listData.push({
        storeId: `front-${currNationalityLang}`,
        name: `${currNationality[0].name} / Popular`,
        slug: `/${currNationalityLang}/radio-stanice`,
        params: {nationalities: currNationalityLang, perPage: 4}
      });

      // randomize genre
      const randGenre = Math.floor(Math.random() * genres.length);
      genres
        .slice(randGenre, (randGenre + 1)).map(g => listData.push({
          storeId: `front-${currNationalityLang}-${g.slug}`,
          name: `${currNationality[0].name} / ${g.name}`,
          slug: `/${currNationalityLang}/radio-stanice?zanr=${g.slug}`,
          params: {nationalities: currNationalityLang, perPage: 4, genres: g.slug}
        }));
      cities
        .filter(c => c.nationality === currNationalityLang)
        .slice(0, 2).map(c => listData.push({
          storeId: `front-${currNationalityLang}-${c.slug}`,
          name: `${currNationality[0].name} / ${c.name}`,
          slug: `/${currNationalityLang}/radio-stanice?lokacija=${c.slug}`,
          params: {nationalities: currNationalityLang, perPage: 4, locations: c.slug}
        }));

    } else {

      // and then create an array of the listData according to available nationalities
      // each entry will have `storeId`, `name` and `slug` properties
      listData = nationalities.map((n) => ({
        storeId: `front-${n.iso}`,
        name: n.name,
        slug: `/${n.lang}`,
        params: { nationalities: n.lang, perPage: 4 }
      }));
    }

    const stationListIds = listData.map(list => {
      store.dispatch(getStationsAction(list.storeId, list.params));
      return list;
    });

    return {
      query,
      stationListIds
    }
  }

  getStations = (list) => {
    const { stations } = this.props;
    return list && list.stationIds ? list.stationIds.map(s => stations[s]) : [];
  }

  render() {
    const { lists, stationListIds, screenWidth } = this.props;
    return (
      <div style={{marginBottom: '150px'}}>
        <div style={{textAlign: 'center'}}>
          <img src={`/static/assets/images/${(screenWidth <= 768 ? '300x250' : '970x250')}.png`} alt="temp" style={{marginBottom: 30}} />
        </div>

        {/* <AdSense
          style={{display: 'inline-block', width: '970px', height: '250px', marginBottom: 35}}
          client="ca-pub-7153790144889102"
          slot="1320463306" test /> */}

        { stationListIds.map(l => <StationGrid key={l.storeId} items={this.getStations(lists[l.storeId])} title={l.name} link={lists[l.storeId].hasNextPage ? l.slug : null} />) }

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  stations: selectStations(state),
  lists: selectStationLists(state),
  defaultNationality: state.cookies[DEFAULT_NATIONALITY_KEY],
  screenWidth: getScreenWidth(state)
});

export default compose(
  connect(mapStateToProps),
  withRoot
)(FrontPage);

// export default withRoot(FrontPage);