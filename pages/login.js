import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../core/withRoot';
import { translate } from 'react-i18next';

export const styles = {
  root: {
    textAlign: 'center',
    maxWidth: '420px',
    margin: '0 auto'
  },
  textField: {
    width: '100%',
    marginBottom: '30px'
  },
  button: {
    marginTop: '20px'
  }
}

class LoginPage extends React.Component {
  // static async getInitialProps({ctx: { store }}) {

  // }

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }

  handleInputChange = name => event => {
    this.setState({
      [name]: event.target.value
    })
  }

  handleFormSubmit = e => {
    e.preventDefault();
    console.log('submitted', this.state);
    console.log('aToken cookie', this.props.aToken);
  }

  render() {
    const { classes, t } = this.props;
    return (
      <div className={classes.root}>
        <form onSubmit={this.handleFormSubmit} noValidate autoComplete="off">
          <div>
          <TextField
            required
            id="username"
            label={t('username')}
            value={this.state.username}
            onChange={this.handleInputChange('username')}
            className={classes.textField}
          />
          </div>

          <div>
          <TextField
            required
            id="password"
            label={t('password')}
            type="password"
            value={this.state.password}
            onChange={this.handleInputChange('password')}
            className={classes.textField}
          />
          </div>

          <Button
            variant="outlined"
            type="submit"
            className={classes.button}>
            {t('submit')}
          </Button>
        </form>
      </div>
    );
  }
}

export default compose(
  withRoot,
  connect(state => ({aToken: state.cookies.aToken})),
  translate(['common']),
  withStyles(styles)
)(LoginPage);