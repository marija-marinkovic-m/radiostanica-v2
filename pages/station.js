import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import withRoot from '../core/withRoot';
import { Grid as StationGrid } from '../components/StationGrid';
import StationCard from '../components/StationList/Card';
import { Favorite, Website, Share, ActionsMenu } from '../components/StationActions';
import SingleNavigation from '../components/SingleNavigation';
import { getStations, getStationById } from '../core/stations/selectors';
import { getStationLists } from '../core/stationLists/selectors';
import { getStation as fetchStation, getStations as fetchStations } from '../core/api/actions';
import { SESSION_STATIONLIST_ID } from '../core/constants';

import { styles as mainStyles } from './list';
import { getScreenWidth } from '../core/mediaQueryTracker/selectors';
import { playSelected } from '../core/player/actions';

const styles = Object.assign({}, mainStyles, {
  root: {marginBottom: '100px'},
  stationCard: {
    marginBottom: '50px'
  }
});

class StationPage extends React.Component {

  static async getInitialProps({ctx: { query, store }}) {
    const stationId = query.id;
    const stationListId = `network-${stationId}`;

    const state = store.getState();
    const backStationListId = state.stationLists.currentStationListId === SESSION_STATIONLIST_ID || state.stationLists.currentStationListId.indexOf('network-') === 0 ? '/' : state.stationLists.currentStationListId;

    const data = getStationById(stationId, state);

    if (!data) {
      store.dispatch(fetchStation(stationId));
    }
    return { query, stationId, stationListId, backStationListId };
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(props) {
    const { stationId, stationListId, stationLists, stations } = props;

    const s = stations[stationId];

    if (s.loading) return null;

    const relatedList = stationLists[stationListId];

    if (relatedList) return null;

    // @TBD: networks


    if (!relatedList && s) {
      const params = {
        include: 'stream,info,details,nationality.lang,links,network,links:active(active_station)',
        // genres: s.genres.map(g => g.slug).join(','),
        locations: s.city,
        // nationalities: s.nationality,
        perPage: 4
      };


      props.dispatch(fetchStations(stationListId, params));
    }

    return null;
  }

  componentDidMount() {
    if (this.props.query.play) {
      const currStation = this.props.stations[this.props.stationId];
      if (currStation && currStation.id) {
        this.props.dispatch(playSelected(currStation, this.props.stationListId));
      }
    }
  }

  relatedStations = (list, stations, currId) => {
    return (!list || !list.stationIds) ? [] : list.stationIds
      .map(id => stations[id])
      .filter(i => i && i.id != currId)
      .slice(0,3);
  }

  render() {
    const { classes, query, stationLists, stations, stationId, stationListId, backStationListId } = this.props;
    const data = stations[stationId];
    const related = stationLists[stationListId];

    return !data || !data.id ? <p>Station not found</p> : (<div className={classes.root}>
      <SingleNavigation
        links={data.links}
        backListId={backStationListId} />
      <Grid container justify="space-between">
        <section className={classes.main}>
          <StationCard
            data={data}
            figureSize={{width: '180px', height: '120px'}}
            playButtonSize="lg"
            typeDisplay
            className={classes.stationCard}>
            <Grid container justify="space-between" alignItems="center">
              <Grid item>
                <Favorite station={data} />
                <Share />
                { data.homepage && <Website href={data.homepage} /> }
              </Grid>
              <ActionsMenu />
            </Grid>
          </StationCard>

          <a id="related" />
          <StationGrid
            title="related"
            items={this.relatedStations(related, stations, stationId)}
            size="sm" />
        </section>
        <aside className={classes.aside}>
          <img src={`/static/assets/images/${(this.props.screenWidth <= 768 ? '300x250' : '300x600')}.png`} alt="temp" />
        </aside>
      </Grid>
    </div>);
  }
}

const mapStateToProps = (state) => ({
  stations: getStations(state),
  stationLists: getStationLists(state),
  screenWidth: getScreenWidth(state)
});

export default compose(
  withRoot,
  withStyles(styles),
  connect(mapStateToProps)
)(StationPage);