import React from 'react';
import App, { Container } from 'next/app';

import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';

import createStore from '../core/store';

import { I18nextProvider } from 'react-i18next';
import startI18n from '../i18n/startI18n';

import { getCookie } from '../core/cookies/actions';
import { setTranslation } from '../core/translations/actions';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { getStoreTranslations } from '../core/translations/selectors';
import { getLocalData } from '../core/localStorage/actions';

import Layout from '../components/Layout';
import { throttle } from '../util/throttle';
import { mediaUpdate } from '../core/mediaQueryTracker/actions';
import { DEFAULT_NATIONALITY_KEY } from '../core/constants';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import { getPlayerAlert } from '../core/player/selectors';
import ErrorComponent from '../components/ErrorComponent';

const defaultLang = 'sr';

class RadioApp extends App {
  state = {
    i18n: null,
    isOpenSnackBar: false,
    msg: '',
    hasError: false
  };
  _throttleResize;

  static async getInitialProps({ Component, ctx }) {
    // initi nat cookie 
    await ctx.store.dispatch(getCookie(ctx, DEFAULT_NATIONALITY_KEY, ''));

    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx });
    }

    if (ctx && ctx.res) {
      // server request, parse cookies
      await ctx.store.dispatch(getCookie(ctx, 'lang', defaultLang));
    }
    return { pageProps };
  }

  constructor(props) {
    super(props);
    this._throttleResize = throttle(this.handleResize, 300, this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.i18n === null) {
      return {
        i18n: startI18n(nextProps.translations, nextProps.lang)
      };
    }
    return null;
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { lang, dispatch, translations } = this.props;      
    
    if (nextProps.lang !== lang) {
       if (!translations
        || Object.keys(translations).indexOf(lang) < 0) {
         // i18n not initializes or lacks lang resources
         dispatch(setTranslation(lang));
       }
    }
    return true;
  }

  componentDidUpdate(prevProps, prevState) {

    // check if translations updated
    if (
        prevProps.lang !== this.props.lang
        || Object.keys(prevProps.translations).length !== Object.keys(this.props.translations).length
      ) {
      if (this.props.lang in this.props.translations) {
        // this.state.i18n.changeLanguage(this.props.lang);
        this.setState({
          i18n: startI18n(this.props.translations, this.props.lang)
        });
      }
    }

    // alerts
    if (this.props.alert !== prevProps.alert && this.props.alert.length) {
      this.setState({
        msg: this.props.alert,
        isOpenSnackBar: true
      });
    }
  }

  componentDidCatch(error, errorInfo) {
    console.log('CUSTOM ERROR HANDLING FROM _app.js', error, errorInfo);
    // needed to render errors correctly in dev / prod
    super.componentDidCatch(error, errorInfo);

    this.setState({hasError: true});
  }

  componentDidMount() {
    // initialize localStorage (if available)
    this.props.dispatch(getLocalData());
    // read screenWidth/height
    this._throttleResize();
    // media query listener
    window.addEventListener('resize', this._throttleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('reseze', this._throttleResize);
  }

  handleResize = () => {
    this.props.dispatch(mediaUpdate())
  }

  closeSnackbar = (e, reason) => {
    if (reason === 'clickaway') return;
    this.setState({isOpenSnackBar: false, msg: ''});
  }

  render() {
    const { Component, pageProps, store } = this.props;

    if (this.state.hasError) return (<Container>
      <Provider store={store}>
        <I18nextProvider i18n={this.state.i18n}>
          <Layout><ErrorComponent /></Layout>
        </I18nextProvider>
      </Provider>
    </Container>);
    return (
      <Container>
        <Provider store={store}>
          <I18nextProvider i18n={this.state.i18n}>
            <Layout q={pageProps.query}>
              <Component {...pageProps} />
              <Snackbar
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right'
                }}
                open={this.state.isOpenSnackBar}
                autoHideDuration={6000}
                onClose={this.closeSnackbar}
                ContentProps={{
                  'aria-describedby': 'new-message'
                }}
                message={<span id="new-message">{this.state.msg}</span>}
                action={[
                  <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    onClick={this.closeSnackbar}>
                    <i className="fas fa-times"></i>
                  </IconButton>
                ]}
              />
            </Layout>
          </I18nextProvider>
        </Provider>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  lang: state.cookies.lang,
  translations: getStoreTranslations(state),
  alert: getPlayerAlert(state)
})

const EnhancedRadioApp = compose(
  withReduxSaga({
    async: true
  }),
  connect(mapStateToProps)
)(RadioApp);

export default withRedux(createStore)(EnhancedRadioApp);

// export default withRedux(createStore)(withReduxSaga({ async: true })(RadioApp));