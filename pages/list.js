import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { Router } from '../routes';

import { withStyles } from '@material-ui/core/styles';
import withRoot from '../core/withRoot';

import { getStations as getStationsAction } from '../core/api/actions';

import { getStations as selectStations } from '../core/stations/selectors';
import { getStationLists as selectStationLists } from '../core/stationLists/selectors';

import { List } from '../components/StationList';
import Pagination from '../components/Pagination';
import Filters from '../components/StationListFilters';
import { DEFAULT_NATIONALITY_KEY, MOBILE_BREAKPOINT } from '../core/constants';
import { setCookie } from '../core/cookies/actions';
import { getScreenWidth } from '../core/mediaQueryTracker/selectors';
// import AdSense from '../components/AdSense';

export const styles = {
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '60px',
    [MOBILE_BREAKPOINT]: {
      flexDirection: 'column'
    }
  },
  main: {
    width: '630px',
    marginBottom: '30px', color: 'red',
    [MOBILE_BREAKPOINT]: {
      width: '100%'
    }
  },
  aside: {
    width: '300px',
    marginBottom: '30px',
    textAlign: 'center',
    '> *': {
      display: 'inline-block'
    },
    [MOBILE_BREAKPOINT]: {
      width: '100%'
    }
  }
}

class ListPage extends React.Component {

  static async getInitialProps({ctx: { query, store, asPath, req, res }}) {

    const defaultNationality = store.getState().cookies[DEFAULT_NATIONALITY_KEY];

    const page = parseInt(query.strana, 10) || 1;
    const locations = query.lokacija || '';
    const genres = query.zanr || '';
    const nationality = query.nationality || defaultNationality;

    if (nationality && nationality !== defaultNationality) {
      store.dispatch(setCookie({req,res}, DEFAULT_NATIONALITY_KEY, nationality));
    }

    const listStoreId = asPath;

    store.dispatch(getStationsAction(listStoreId, {
      nationalities: nationality,
      locations,
      genres,
      page
    }));

    return { listStoreId, query, page };
  }

  shouldComponentUpdate(nextProps) {
    // if (
    //   nextProps.query.lokacija == this.props.query.lokacija
    //   && nextProps.query.zanr == this.props.query.zanr
    //   && nextProps.query.strana == this.props.query.strana
    // ) return false;
    if (nextProps.defaultNationality !== this.props.defaultNationality) return false;
    if (nextProps.listStoreId !== this.props.listStoreId) return false;
    return true;
  }

  componentDidUpdate(prevProps) {
    if (this.props.query.nationality !== this.props.defaultNationality) {
      this.props.dispatch(setCookie(null, DEFAULT_NATIONALITY_KEY, this.props.query.nationality));
    }
  }

  getStations = (list) => {
    const { stations } = this.props;
    return list && list.stationIds ? list.stationIds.map(s => stations[s]) : [];
  }

  handlePaginate = ({selected}) => {
    if (selected === this.props.page - 1) return;
    const currRoute = Boolean(this.props.query.nationality) ? 'list' : 'radio-stanice';
    Router.pushRoute(currRoute, Object.assign({}, this.props.query, {strana: (selected + 1)}));
  }

  renderPagination = (list) => {
    const { currentPage, pageCount } = list;
    return pageCount <= 1 ? null : <Pagination
      pageCount={pageCount}
      pageRangeDisplayed={5}
      marginPagesDisplayed={1}
      initialPage={this.props.page - 1}
      onPageChange={this.handlePaginate} />
  }

  render() {
    const { lists, listStoreId:id, classes, query, screenWidth } = this.props;
    const stations = this.getStations(lists[id]);

    const ThePagination = this.renderPagination.bind(null, lists[id]);

    return (<React.Fragment>
      <Filters parentParams={query} />
      <div className={classes.root}>
        <section className={classes.main}>
          
          <List items={stations} />
          <ThePagination />

        </section>
        <aside className={classes.aside}>
          <img src={`/static/assets/images/${(screenWidth > 768 ? '300x600' : '300x250')}.png`} alt="temp" />
          {/* <AdSense
            style={{display: 'inline-block', width: '300px', height: '600px'}}
            client="ca-pub-7153790144889102"
            slot="1320463306" test /> */}
        </aside>
      </div>
    </React.Fragment>);
  }
}

const mapStateToProps = (state) => ({
  stations: selectStations(state),
  lists: selectStationLists(state),
  screenWidth: getScreenWidth(state),
  defaultNationality: state.cookies[DEFAULT_NATIONALITY_KEY]
});

export default compose(
  connect(mapStateToProps),
  withStyles(styles),
  withRoot
)(ListPage);