import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../core/withRoot';

import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {textAlign: 'center'}
});

class ErrorPage extends React.Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode }
  }

  render() {
    const { classes, statusCode } = this.props;
    return (
      <div className={classes.root}>
        <Typography variant="subheading">
          {statusCode
            ? `An error ${statusCode} occurred on server`
            : 'An error occurred on client'}
        </Typography>
      </div>
    )
  }
}

ErrorPage.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withRoot(withStyles(styles)(ErrorPage));

