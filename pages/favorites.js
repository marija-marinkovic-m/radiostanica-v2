import React from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { translate } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './list';
import withRoot from '../core/withRoot';
import { FAVORITES_STORAGE_KEY } from '../core/constants';
import { List } from '../components/StationList';
import CircularProgress from '@material-ui/core/CircularProgress';
import { setLocalData as setLocalDataAction } from '../core/localStorage/actions';
import { fetchStationsSuccess as loadStationListAction } from '../core/stationLists/actions';

import { api } from '../core/api/api-service';
import { getScreenWidth } from '../core/mediaQueryTracker/selectors';

// import AdSense from '../components/AdSense';

function diffMinutes (date1, date2) {
  let diff = (date2.getTime() - date1.getTime()) / 1000;
  diff /= 60;
  return Math.abs(Math.round(diff));
}

class FavoritesPage extends React.Component {
  state = {
    list: null
  }

  static getDerivedStateFromProps(props, state) {
    const { storageData } = props;
    if (!storageData) return null;

    if (!storageData[FAVORITES_STORAGE_KEY]) return {list: []};

    let list = storageData[FAVORITES_STORAGE_KEY];

    // normalize storage data
    try {
      list = JSON.parse(list);
    } catch(e) {
      // console.log('error reading storage')
    }
    if (typeof list === 'object') {
      list = Object.keys(list).map(k => list[k]);
    }

    if (!list.length) return {list};

    if (!state.list || state.list.length !== list.length) {
      // save list to stationList (store)
      props.loadStationList('/favorites', {collection: list});

      // check each item
      // `latestFavCheck` storage prop (sync timestamp)
      // check stations validity and remove all inactive/broken/deleted ones
      const today = new Date();
      let latestSync;
      try {
        latestSync = JSON.parse(storageData.latestFavCheck);
      } catch(e) {
        latestSync = storageData.latestFavCheck;
      }

      if (!latestSync || diffMinutes(new Date(latestSync), today) >= 1440) { // 1440 once a day
        console.log('syncing favorites...');

        Promise.all(list.map(i => api.fetchStation(i.id, {include: null})))
          .then((results) => {
            const updatedFavs = results
              .map((item, index) => {
                if (!item || item.active !== 'active_station') {
                  return false;
                }
                return list[index];
              })
              .filter(el => el)
              .reduce((acc, n) => {
                acc[n.id] = n;
                return acc;
              }, {});
            
            console.log('updatedFAvs', updatedFavs);
            props.setLocalData({key: 'latestFavCheck', value: today});
            props.setLocalData({key: FAVORITES_STORAGE_KEY, value: updatedFavs});
          });
      }

      return {list};
    }

    return null;
  }

  render() {
    const { t, classes, storageData, screenWidth } = this.props;
    const { list } = this.state;

    return (<div className={classes.root}>
      <section className={classes.main}>

        { list === null && <CircularProgress /> }
      
        { list && list.length === 0 ? <p>{t('no favorites')}</p> : (
          <List items={list} />
        ) }

      </section>
      <aside className={classes.aside}>
        <img src={`/static/assets/images/${(screenWidth <= 768 ? '300x250': '300x600')}.png`} alt="temp" />
        {/* <AdSense
          style={{display: 'inline-block', width: '300px', height: '600px'}}
          client="ca-pub-7153790144889102"
          slot="1320463306" test /> */}
      </aside>
    </div>);
  }
}

const mapDispatchToProps = (dispatchEvent) => ({
  setLocalData: bindActionCreators(setLocalDataAction, dispatchEvent),
  loadStationList: bindActionCreators(loadStationListAction, dispatchEvent)
});

export default compose(
  withRoot,
  connect((s) => ({storageData: s.localStorage.data, screenWidth: getScreenWidth(s)}), mapDispatchToProps),
  translate(['common']),
  withStyles(styles)
)(FavoritesPage);