import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { translate } from 'react-i18next';
import { createSelector } from 'reselect';
import { withRouter } from 'next/router';

import cn from 'classnames';
import { Spring, animated } from 'react-spring';
import { Link, Router } from '../../routes';

import ImgTag from '../shared/ImgTag';
import PlayButton from '../shared/PlayButton';
import InputRange from'../InputRange';

import { getPlayer } from '../../core/player/selectors';
import { getStations } from '../../core/stations/selectors';
import { getStationsForCurrentStationList } from '../../core/stationLists/selectors';

import {
  setVolume as setVolumeAction, playSelected, pauseAudio
} from '../../core/player/actions';
import { getScreenHeight } from '../../core/mediaQueryTracker/selectors';
import StationActivityBar from '../shared/StationActivityBar';

const noop = () => {};

function navigateTo(url, props = ['_blank']) {
  window && window.open(url, ...props);
}

const ChevronDownButton = ({station, expanded, toggleExpand = noop}) => (
  <button
    className="collapse-btn"
    type="button"
    onClick={toggleExpand}>
    <i className="fas fa-chevron-down fa-fw"></i>
  </button>
);

const StationInfo = ({station, expanded, toggleExpand = noop}) => (<div
  onClick={expanded ? noop : toggleExpand}
  className="station-info">
  <h3>{station.title}</h3>
  <StationActivityBar
    id={parseInt(station.id, 10)}
    mode="compact"
    alt={`${station.city}, ${station.homepage}`} />
</div>);

const Tags = ({station, expanded, toggleExpand = noop}) => {
  const tags = [{
      asPath: `/${station.nationality}/radio-stanice?lokacija=${station.city}`,
      name: station.city
    }]
    .concat(!station.genres ? [] : station.genres.map(g => ({
      asPath: `/${station.nationality}/radio-stanice?zanr=${g.slug}`,
      name: g.name
    })));

  return (<ul className="tags-wrap">
    {tags.map((t,i) => <li key={i} onClick={toggleExpand}><Link route={t.asPath}><a>{t.name}</a></Link></li>)}
  </ul>);
};

const Controls = ({station, expanded, prev, next, forceplay = false, toggleExpand = noop}) => {
 
  return (<div className="controls-wrap">
    { expanded && <React.Fragment>
      <button onClick={navigateTo.bind(null, 'mailto:test@mail.com')} className="ctrl" type="button">
        <i className="fas fa-exclamation-triangle fa-fw"></i>
      </button>
      { prev && <Link route={prev}><button className="ctrl border" type="button">
        <i className="fas fa-chevron-left fa-fw"></i> 
      </button></Link> }
      
    </React.Fragment> }
    <Spring
      from={{width: 30, height: 30, fontSize: 10}}
      to={{width: expanded ? 70 : 30, height: expanded ? 70 : 30, fontSize: expanded ? 30 : 10}}>
      { (props) => (<PlayButton
        station={station}
        forceplay={forceplay.toString()}
        style={props}
      />) }
    </Spring>
    {
      expanded && next && <Link route={next}><button className="ctrl border" type="button">
        <i className="fas fa-chevron-right fa-fw"></i>
      </button></Link>
    }
    <button onClick={_ => window.location.assign(`/radio/${station.id}/${station.slug}?play=1`)} className="ctrl" type="button">
      <i className="fas fa-sync fa-fw"></i>
    </button>
  </div>);
}

const VolumeControls = ({volume, setVolume}) => (<div className="controls-wrap volume">
  <button
    type="button"
    className="ctrl"
    onClick={setVolume.bind(null, (volume - 0.1))}>
    <i className="fas fa-volume-off"></i>
  </button>
  <InputRange
    maxValue={1}
    minValue={0}
    step={0.05}
    value={volume}
    onChange={setVolume} />
  <button
    type="button"
    className="ctrl"
    onClick={setVolume.bind(null, (volume + 0.1))}>
    <i className="fas fa-volume-up"></i>
  </button>
</div>)

const LogoFigure = ({station, expanded, toggleExpand = noop, ...styles}) => (
  <ImgTag
    src={station.artworkUrl}
    render={src => <figure
      className="logo-figure"
      onClick={expanded ? noop : toggleExpand}
      style={styles}>
      <img src={src} />
    </figure>}
  />
);

/**
 * Functional, statefull Player Component.
 * 
 * Used to render selected station info. Update player preferences and dispatch audio streaming.
 * ```javascript
 * <Player query={q} />
 * ```
 */
class Player extends React.Component {
  static propTypes = {
    /**
     * Player object form redux store, shape `{isPlaying: boolean; isReady: boolean; stationId: number; stationListId: string; volume: number; initialized: boolean; alert: string}`
     */
    player: PropTypes.object,
    /**
     * Stations object from redux store
     */
    stations: PropTypes.object,
    /**
     * Method to update audio volume
     */
    setVolume: PropTypes.func,
    /**
     * Method to dispath play selected station action
     */
    play: PropTypes.func
  }
  state = {
    expanded: false
  }

  componentDidUpdate(prevProps) {
    const queryId = this.props.router.query.id;

    if (!this.props.player.stationId || prevProps.player.stationId === queryId) return;

    this.props.play({
      id: queryId
    });
  }

  static getDerivedStateFromProps(props) {
    return {expanded: Boolean(props.router.query.exp)}
  }

  toggleExpand = () => this.setState((prevState) => ({
    expanded: !prevState.expanded
  }))

  render() {
    const { station, screenHeight, player, setVolume, router } = this.props;
    const { expanded } = this.state;

    const childProps = {
      station, expanded,
      toggleExpand: this.toggleExpand
    }

    return !station || !station.id ? null : (
      <Spring
        native
        from={{height: 60}}
        to={{height: expanded ? screenHeight : 60}}>
        { (props) => (
          <animated.div
            className={cn(['player'], {expanded})} style={props}>

            { expanded && <ChevronDownButton {...childProps} /> }

            <div className="flex-wrap">
              <Spring
                render={LogoFigure}
                from={{width: 40, height: 40}}
                to={{width: expanded ? 300 : 40, height: expanded ? 250 : 40}}
                {...childProps}
              />

              <StationInfo {...childProps} />
            </div>

            { expanded && <Tags {...childProps} /> }

            <Controls
              prev={station.links.prev}
              next={station.links.next}
              forceplay={Boolean(router.query.play)}
              {...childProps}
            />

            { expanded && <VolumeControls
              volume={player.volume}
              setVolume={setVolume}
            /> }

          </animated.div>
        ) }
      </Spring>
    );
  }
}

const mapStateToProps = (state) => createSelector(
  getPlayer,
  getStations,
  getStationsForCurrentStationList,
  getScreenHeight,
  (player, stations, currentListStations, screenHeight) => ({
    player, screenHeight, currentListStations, stations,
    station: stations && stations[player.stationId] || {}
  })
);

const mapDispatchToProps = dispatchEvent => ({
  setVolume: bindActionCreators(setVolumeAction, dispatchEvent),
  play: bindActionCreators(playSelected, dispatchEvent),
  stop: bindActionCreators(pauseAudio, dispatchEvent)
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  translate(['common']),
  withRouter
)(Player);