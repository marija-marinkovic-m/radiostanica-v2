import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

/**
 * Google AdSense component 
 * 
 * This component renders the Google AdSense code for showing AdSense banners.
 * ```javascript
 * // ads with no set-up
 * <AdSense
 *   client='ca-pub-7292810486004926'
 *   slot='7806394673'
 * />
 * 
 * // ads with custom format
 * <AdSense
 *   client='ca-pub-7292810486004926'
 *   slot='7806394673'
 *   style={{ width: 500, height: 300, float: 'left' }}
 *   format=''
 * />
 * 
 * // responsive and native ads
 * <AdSense
 *   client='ca-pub-7292810486004926'
 *   slot='7806394673'
 *   style={{ display: 'block' }}
 *   layout='in-article'
 *   format='fluid'
 * />
 * ```
 */
class AdSense extends React.Component {
  componentDidMount() {
    if (window) (window.adsbygoogle = window.adsbygoogle || []).push({});
  };

  render() {
    const testProp = this.props.test ? {'data-ad-test': 'on'} : {};
    const addedClasses = this.props.className ? this.props.className.split(' ') : [];
    return (
      <ins
        className={cn(['adsbygoogle', ...addedClasses])}
        style={this.props.style}
        data-ad-client={this.props.client}
        data-ad-slot={this.props.slot}
        data-ad-layout={this.props.layout}
        data-ad-format={this.props.format}
        {...testProp}></ins>
    );
  }
};

AdSense.propTypes = {
  /**
   * React.Component `className` prop
   */
  className: PropTypes.string,
  /**
   * React.Component `style` prop
   */
  style: PropTypes.object,
  /**
   * AdSense client ID
   */
  client: PropTypes.string.isRequired,
  /**
   * AdSense slot ID
   */
  slot: PropTypes.string.isRequired,
  /**
   * AdSense layout enum.
   */
  layout: PropTypes.string,
  /**
   * AdSense format enum
   */
  format: PropTypes.oneOf(['auto', 'vertical', 'horizontal', 'rectangle', 'fluid']),
  /**
   * Should indicate testing
   */
  test: PropTypes.bool
};

AdSense.defaultProps = {
  className: '',
  style: { display: 'block' },
  layout: '',
  format: 'auto',
  test: false
};

export default AdSense;