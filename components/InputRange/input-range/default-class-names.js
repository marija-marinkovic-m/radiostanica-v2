import cn from 'classnames';

/**
 * Default CSS class names
 * @ignore
 * @type {InputRangeClassNames}
 */
const DEFAULT_CLASS_NAMES = {
  activeTrack: cn(['input-range__track', 'input-range__track--active']),
  disabledInputRange: cn(['input-range', 'input-range--disabled']),
  inputRange: 'input-range',
  slider: 'input-range__slider',
  sliderContainer: 'input-range__slider-container',
  track: cn(['input-range__track', 'input-range__track--background'])
};

export default DEFAULT_CLASS_NAMES;
