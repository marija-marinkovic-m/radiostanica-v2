import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '../../routes';
import cn from 'classnames';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import ImgTag from '../shared/ImgTag';
import ActivityBar from '../shared/StationActivityBar';

import PlayButton from '../shared/PlayButton';
import StationMeta from '../shared/StationMetaLinks';
import { getScreenWidth } from '../../core/mediaQueryTracker/selectors';
import { MOBILE_BREAKPOINT } from '../../core/constants';

const styles = (theme) => ({
  root: {
    overflow: 'hidden',
    clear: 'both',
    border: '1px solid #EAEAEA',
    transition: 'box-shadow 200ms ease-in',
    '&:active': {
      boxShadow: '0 1px 2px rgba(43,59,93,.15), 0 8px 16px 0 rgba(0,0,0,.2)'
    },
    [MOBILE_BREAKPOINT]: {
      marginBottom: '20px',
      borderLeft: 'none', borderRight: 'none'
    }
  },
  figure: {
    margin: '20px 0 20px 20px', padding: '0',
    '& > img': {
      display: 'block',
      maxWidth: '100%', height: 'auto'
    },
    [MOBILE_BREAKPOINT]: {
      marginLeft: '15px'
    }
  },
  infoBox: {
    marginLeft: '30px',
    [MOBILE_BREAKPOINT]: {
      marginLeft: '20px'
    }
  },
  dataWrap: {
    display: 'flex', justifyContent: 'space-between',
    alignItems: 'center',
    overflow: 'hidden',
    [MOBILE_BREAKPOINT]: {
      paddingTop: '5px'
    }
  },
  playButton: {
    whiteSpace: 'nowrap',
    margin: '20px'
  },
  title: {
    fontWeight: 'bold', fontSize: '16px',
    '& > a': {
      color: 'inherit', textDecoration: 'none',
      '&:hover': {
        textDecoration: 'underline'
      }
    }
  }
});

class StationCard extends React.Component {

  renderFigure = (src) => (
    <Link route="station" params={{id: this.props.data.id, slug: this.props.data.slug || null}}><a>
      <figure
        className={this.props.classes.figure}
        style={this.props.screenWidth <= 768 ? {width: '100px', height: '66px'} : this.props.figureSize}>
        <img
          src={src}
          alt={this.props.data.title}
        />
      </figure>
    </a></Link>
  );

  render() {
    const { data, classes, children, className } = this.props;
    const addedClasses = className ? className.split(' ') : [];
    return !data ? null : (<Paper
        className={cn([classes.root, ...addedClasses])}
        elevation={0}>

        <div className={classes.dataWrap}>
          <article className={classes.dataWrap}>
            <ImgTag
              src={data.artworkUrl}
              render={this.renderFigure}
            />
            <div className={classes.infoBox}>
              
              <Typography
                noWrap gutterBottom
                component="h3"
                className={classes.title}>
                <Link route="station" params={{id: data.id, slug: data.slug || null}}><a>{data.title}</a></Link>
              </Typography>
              
              <StationMeta data={data} typeDisplay={this.props.typeDisplay} />

            </div>
          </article>

          <div>
            <PlayButton
              station={data}
              className={classes.playButton}
              size={this.props.playButtonSize} />
          </div>

        </div>

        <ActivityBar
          id={parseInt(data.id,10)}
          alt={`${data.city}, ${data.homepage}`} />

        {children}
      </Paper>);
  }
}

StationCard.propTypes = {
  data: PropTypes.object.isRequired,
  figureSize: PropTypes.shape({width: PropTypes.string, height: PropTypes.string}),
  playButtonSize: PropTypes.oneOf(['lg', 'md', 'sm']),
  typeDisplay: PropTypes.bool,
  classes: PropTypes.object.isRequired,
  screenWidth: PropTypes.number.isRequired
}
StationCard.defaultProps = {
  figureSize: {width: '120px', height: '80px'},
  playButtonSize: 'md',
  typeDisplay: false
}

export default compose(
  withStyles(styles),
  connect(state => ({screenWidth: getScreenWidth(state)}))
)(StationCard);