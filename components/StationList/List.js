import React from 'react';
import PropTypes from 'prop-types';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Zoom from '@material-ui/core/Zoom';
import { withStyles } from '@material-ui/core/styles';

import StationCard from './Card';

const styles = {
  root: {
    marginBottom: '40px'
  },
  tile: {
    overflow: 'visible'
  }
};

class StationList extends React.PureComponent {
  render() {
    const { items, animate, classes } = this.props;
    return !items || !items.length ? null : (
      <div className={classes.root}>
        <GridList
          spacing={24}
          cols={1}
          cellHeight="auto">
          { items.map(s => <Zoom in={true} timeout={animate ? 225 : 0} key={s.id}>
            <GridListTile classes={{tile: classes.tile}}><StationCard data={s} /></GridListTile>
          </Zoom>) }
        </GridList>
      </div>
    );
  }
}

StationList.propTypes = {
  items: PropTypes.array,
  animate: PropTypes.bool,
  classes: PropTypes.object.isRequired
}

StationList.defaultProps = {
  items: [],
  animate: true
}

export default withStyles(styles)(StationList);