import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getPlayer } from '../../core/player/selectors';
import { playSelected, pauseAudio } from '../../core/player/actions';
import { getScreenWidth, getScreenHeight } from '../../core/mediaQueryTracker/selectors';

class PlayButton extends React.Component {
  _curr = false;

  constructor(props) {
    super(props);
    this._curr = props.player.stationId == props.station.id;
    this.state = {
      loading: false
    }
  }

  componentDidMount() {
    if (this.props.forceplay) {
      this.props.play(this.props.station);
      this._curr = true;
    }
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.player.stationId !== this.props.player.stationId
      || prevProps.station.id !== this.props.station.id
    ) {
      this._curr = this.props.player.stationId == this.props.station.id;

      this.setState({loading: false})
    }

    if (this.props.player.isPlaying && this.state.loading) {
      this.setState({loading: false})
    }
  }

  doAction = () => {
    const { screenheight, screenwidth, station, player } = this.props;
    
    if (!this._curr ||
      !player.isPlaying
      // && this.props.player.isReady
    ) {
      if (screenwidth > 768 && !player.stationId) {
        const path = `/radio/${station.id}/${station.slug}`;
        const width = 386;
        const height = 655;
        const left = screenwidth / 2 - width / 2;
        const top = screenheight / 2 - height / 2;

        const newWindow = window.open(`${path}?play=1`, station.title, `width=${width},height=${height},scrollbars,left=${left},top=${top}`);

        return newWindow.focus();
      } else {
        return this.props.play(this.props.station);
      }
    }
    return this.props.pause();
  }

  handleClick = () => {
    this.setState({loading: true}, () => {
      this.doAction();
      this.setState({loading: false});
    });
  }
  render() {
    const {
      size, station, play, pause,
      player: { initialized, isReady, isPlaying, alert },
      className,
      ...other
    } = this.props;

    const { loading } = this.state;

    const addedClasses = !className ? [] : className.split(' ');

    return !initialized ? null : (
      <button
        {...other}
        onClick={this.handleClick}
        type="button"
        className={cn(['play-button', size, ...addedClasses])}>
        <i
          className={cn(
            ['fas'],
            {
              'fa-play': !this._curr || !loading && !isPlaying && !alert,
              'fa-pause': this._curr && (!loading && isReady && isPlaying),
              'fa-spinner fa-spin': loading || (this._curr && !isReady && (isPlaying || !alert)),
              'fa-exclamation': this._curr && alert
            }
          )}>
        </i>
      </button>
    );
  }
}

PlayButton.propTypes = {
  station: PropTypes.shape({
    streamUrl: PropTypes.string
  }).isRequired,
  forceplay: PropTypes.any,
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
  player: PropTypes.shape({
    isReady: PropTypes.bool,
    isPlaying: PropTypes.bool,
    stationId: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
  }).isRequired,
  play: PropTypes.func.isRequired,
  pause: PropTypes.func.isRequired,
  screenwidth: PropTypes.number.isRequired,
  screenheight: PropTypes.number.isRequired
}

PlayButton.defaultProps = {
  size: 'md',
  forceplay: null
}

const mapStateToProps = (state) => ({
  player: getPlayer(state),
  screenwidth: getScreenWidth(state),
  screenheight: getScreenHeight(state)
})
const mapDispatchToProps = (dispatch) => ({
  play: bindActionCreators(playSelected, dispatch),
  pause: bindActionCreators(pauseAudio, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(PlayButton);