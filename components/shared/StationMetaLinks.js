import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '../../routes';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    '& a': {
      textDecoration: 'none',
      color: 'inherit',
      '&:hover': {
        textDecoration: 'underline'
      }
    }
  }
};

const StationMetaLinks = ({data, typeDisplay, classes}) => {
  const place = {
    asPath: `/${data.nationality}/radio-stanice?lokacija=${data.city}`,
    name: data.city
  };
  const genres = data.genres && data.genres.map(g => ({
    asPath: `/${data.nationality}/radio-stanice?zanr=${g.slug}`,
    name: g.name
  }));

  return (
    <div className={classes.root}>
      { place && place.name ? (
        <Typography component="p" noWrap gutterBottom>
          <Link route={place.asPath}><a>{place.name}</a></Link>
        </Typography>
      ) : null }
      { genres && genres.length ? (
        <Typography component="p" noWrap gutterBottom>
          {
            genres
              .map((genre, i) => <Link key={i} route={genre.asPath}><a>{ genre.name }</a></Link>)
              .reduce((acc, n) => [acc, ', ', n])
          }
        </Typography>
      ) : null }

      { typeDisplay && data.typeName ? (
        <Typography component="p" noWrap>
          <a href="#related">{data.typeName}&nbsp;{data.typeF}</a>
        </Typography>
      ) : null }
    </div>
  );
}

StationMetaLinks.propTypes = {
  data: PropTypes.shape({
    nationality: PropTypes.string,
    city: PropTypes.string,
    genres: PropTypes.array
  }).isRequired,
  typeDisplay: PropTypes.bool,
  classes: PropTypes.object.isRequired
}

StationMetaLinks.defaultProps = {
  typeDisplay: false
}

export default withStyles(styles)(StationMetaLinks);