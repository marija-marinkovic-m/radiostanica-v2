import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import { compose } from 'redux';
import { translate } from 'react-i18next';
import { MOBILE_BREAKPOINT } from '../../core/constants';
import { api } from '../../core/api/api-service';

const styles = () => ({
  root: {
    display: 'flex', justifyContent: 'space-between',
    marginTop: '25px', paddingLeft: '13px', paddingRight: '13px',
    backgroundColor: '#F3F3F3',
    [MOBILE_BREAKPOINT]: {
      marginTop: '15px'
    }
  },
  text: {
    fontSize: '12px', color: '#7A8587', lineHeight: '30px', textAlign: 'inherit',
    '& > strong': {
      color: '#737373'
    },
    [MOBILE_BREAKPOINT]: {
      fontSize: '10px'
    }
  }
});

class StationActivityBar extends React.PureComponent {
  state = {
    track: '',
    listeners: ''
  }

  componentDidMount() {
    this._isMounted = true;
    if (!this.props.id) return;
    this.getTrackInfo();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.id !== this.props.id) {
      this.getTrackInfo();
    }
  }

  getTrackInfo = () => {
    api.trackInfo(this.props.id)
      .then(res => {
        // console.log(`[GET] http://api.radio.etl.yt/tracks-info/${this.props.id}`, res);

        if (!this._isMounted) return;
        if (res && res.data) {
          this.setState({
            ...res.data
          });
        }
      });
  }

  renderTrack = (isCompact, track) => {
    const { t, classes } = this.props;
    return isCompact ? (
      <p>{track}</p>
    ) : (
      <Typography noWrap className={classes.text}>
        <strong>{t('now')} &#9658;</strong> {track}
      </Typography>
    )
  }

  renderAlt = (isCompact, alt) => {
    const { classes } = this.props;
    return isCompact ? (
      <p>{alt}</p>
    ) : (
      <Typography noWrap className={classes.text}>{alt}</Typography>
    );
  }

  render() {
    const { track, listeners } = this.state;
    const { classes, alt, mode } = this.props;

    const isCompact = mode === 'compact';

    return !track && !listeners && !alt ? null : (
      <div className={!isCompact ? classes.root : ''}>
        { track && this.renderTrack(isCompact, track) }

        { !track && alt && this.renderAlt(isCompact, alt) }

        { !isCompact && listeners > 0 && <Typography noWrap className={classes.text}><strong><i className="fas fa-headphones"></i></strong>&nbsp;{listeners}</Typography> }
      </div>
    );
  }
}

StationActivityBar.propTypes = {
  t: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  id: PropTypes.number,
  mode: PropTypes.oneOf(['normal', 'compact'])
}

StationActivityBar.defaultProps = {
  id: null,
  alt: '',
  mode: 'normal'
}

export default compose(
  translate(['common']),
  withStyles(styles)
)(StationActivityBar);