import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '../../routes';
import { compose } from 'redux';
import { translate } from 'react-i18next';

import Grid from '@material-ui/core/Grid';
import Zoom from '@material-ui/core/Zoom';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';

import Station from './Card';
import { MOBILE_BREAKPOINT } from '../../core/constants';

const styles = {
  gridContainter: {
    marginBottom: 60,
    [MOBILE_BREAKPOINT]: {
      marginBottom: 30,
      paddingLeft: '12.5px', paddingRight: '12.5px'
    }
  },
  headline: {
    color: '#373A3C', fontSize: '24px', fontWeight: 'bold',
    [MOBILE_BREAKPOINT]: {
      fontSize: '20px',
      paddingLeft: '12.5px', paddingRight: '12.5px'
    }
  },
  link: {
    color: '#55ACEE', fontSize: '16px', textDecoration: 'none',
    [MOBILE_BREAKPOINT]: {
      fontSize: '13px',
      paddingLeft: '12.5px', paddingRight: '12.5px'
    }
  }
}


class StationsGrid extends React.Component {
  renderHeader = (title, link) => {
    const { classes } = this.props;
    return (<Grid
      container
      justify="space-between"
      alignItems="center"
      style={{marginBottom: 20}}>
      { title && <Typography
        className={classes.headline}
        variant="headline"
        component="h2">
        {this.props.t(title)}
      </Typography> }
      { link && <Link route={link}>
        <a className={classes.link}>{this.props.t('see all')} &rsaquo;</a>
      </Link> }
    </Grid>);
  }
  render() {
    const { items, size, animate, classes, link, title } = this.props;
    const sizeProps = size === 'md' ? {sm: 6, md: 3} : {sm: 2, md: 4};

    return !items || !items.length ? null : (
      <React.Fragment>
        { (title || link) && this.renderHeader(title, link) }
        <Grid
          container
          spacing={24}
          classes={{container: classes.gridContainter}}>
          {items.map(s => <Zoom in={true} timeout={animate ? 255 : 0} key={s.id}>
            <Grid item xs={6} {...sizeProps}>
              <Station data={s} />
            </Grid>
          </Zoom>)}
        </Grid>
      </React.Fragment>
    );
  }
}

StationsGrid.propTypes = {
  items: PropTypes.array.isRequired,
  size: PropTypes.oneOf(['md', 'sm']),
  animate: PropTypes.bool,
  title: PropTypes.string,
  link: PropTypes.string,
  classes: PropTypes.object.isRequired,
  t: PropTypes.func.isRequired
}

StationsGrid.defaultProps = {
  size: 'md',
  animate: true
}

export default compose(
  translate(['common']),
  withStyles(styles)
)(StationsGrid);