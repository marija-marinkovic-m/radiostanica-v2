import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '../../routes';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import ImgTag from '../shared/ImgTag';
import ActivityBar from '../shared/StationActivityBar';
import { MOBILE_BREAKPOINT } from '../../core/constants';

const styles = (theme) => ({
  root: {
    overflow: 'hidden',
    textAlign: 'center',
    border: '1px solid #EAEAEA',
    transition: 'box-shadow 200ms ease-in',
    '&:hover, &:active, &:focus': {
      boxShadow: '0 1px 2px rgba(43,59,93,.15), 0 8px 16px 0 rgba(0,0,0,.2)'
    }
  },
  anchor: {
    textDecoration: 'none',
    color: 'inherit'
  },
  figure: {
   maxWidth: '100%', height: '120px',
   margin: '25px',
   backgroundPosition: 'center', backgroundSize: 'contain',
   backgroundRepeat: 'no-repeat',
   [MOBILE_BREAKPOINT]: {
     height: '100px', margin: '10px'
   }
  },
  title: {
    fontWeight: 'bold', fontSize: '16px'
  },
  location: {
    color: '#979797', fontSize: '12px'
  }
});

class Station extends React.PureComponent {
  renderFigure = (src) => (<figure
    className={this.props.classes.figure}
    style={{backgroundImage: `url(${src})`}}>
  </figure>);
  render() {
    const { data, classes } = this.props;

    return (<Paper className={classes.root} elevation={0}>
      <Link route="station" params={{id: data.id, slug: data.slug}}>
        <a className={classes.anchor}>
          <ImgTag
            src={data.artworkUrl}
            render={this.renderFigure}
          />

          <Typography
            noWrap gutterBottom
            component="h3"
            className={classes.title}>
            {data.title}
          </Typography>
          <Typography
            className={classes.location}
            component="p"
            gutterBottom paragraph noWrap>
            {data.city}
          </Typography>

          {/* <pre>{data.streamUrl}</pre> */}

          <ActivityBar
            id={parseInt(data.id,10)}
            alt={`${data.city}, ${data.homepage}`}
             />
        </a>
      </Link>
    </Paper>)
  }
}

Station.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired
}

export default withStyles(styles)(Station);