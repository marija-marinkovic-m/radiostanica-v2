import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { setCookie as setCookieAction } from '../core/cookies/actions';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { Button } from '@material-ui/core';

const availableLngs = {
  en: 'english',
  sr: 'serbian'
}

/**
 * This component enables user to switch between available tranlations. 
 * 
 * It dispatches `setCookie` appstore action on demand, thus updating selected language globaly.
 * ```javascript
 * // usage
 * <LangSwitcher />
 * ```
 */
class LangSwitcher extends React.Component {
  static propTypes = {
    /**
     * Selected language identifier
     */
    lang: PropTypes.string,
    /**
     * Language switcher function
     */
    setCookie: PropTypes.func
  }
  switchLang = lang => this.props.setCookie(null, 'lang', lang);
  render() {
    const { t, lang } = this.props;

    return (
      <div>
        <p>{t('change_lang')}</p>
        {
          Object.keys(availableLngs)
            .map(l => <Button key={l} onClick={this.switchLang.bind(null, l)}>
              {l === lang && '✔'}&nbsp;
              {t(availableLngs[l])}
            </Button>)
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  lang: state.cookies.lang
});
const mapDispatchToProps = dispatchEvent => ({
  setCookie: bindActionCreators(setCookieAction, dispatchEvent)
});

const enhance = compose(
  translate(['common']),
  connect(mapStateToProps, mapDispatchToProps)
);

export default enhance(LangSwitcher);