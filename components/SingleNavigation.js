import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';

import { Link } from '../routes';

import Grid from '@material-ui/core/Grid';
import { getStations } from '../core/stations/selectors';
import { getStationLists } from '../core/stationLists/selectors';
import { MOBILE_BREAKPOINT } from '../core/constants';

const styles = {
  root: {
    width: '630px', marginBottom: '30px',
    '& a': {
      textDecoration: 'none', color: '#55ACEE'
    },
    [MOBILE_BREAKPOINT]: {
      width: '100%',
      paddingLeft: '10px', paddingRight: '10px'
    }
  }
}

class SingleNavigation extends React.Component {
  state = {
    back: null
  }

  static getDerivedStateFromProps(props) {
    const { backListId } = props;

    return {
      back: backListId.indexOf('/') === 0 ? backListId : '/'
    }
  }

  render() {
    const { back } = this.state;
    const { t, classes, links } = this.props;

    return !back || !links ? null : (
      <Grid container justify="space-between" className={classes.root}>
        <Link route={back}><a>&larr; {t('back')}</a></Link>

        <Grid item>
          { links.prev && <Link route={links.prev}><a>&lsaquo; {t('previous')}</a></Link> }
          { links.next && <>&nbsp;&nbsp;&nbsp;<Link route={links.next}><a>{t('next')} &rsaquo;</a></Link></> }
        </Grid>
      </Grid>
    );
  }
}

SingleNavigation.propTypes = {
  backListId: PropTypes.string.isRequired,
  links: PropTypes.shape({prev: '', next: ''})
};

const mapStateToProps = (state) => ({
  stationLists: getStationLists(state),
  stations: getStations(state)
});

export default compose(
  connect(mapStateToProps),
  translate(['common']),
  withStyles(styles)
)(SingleNavigation);