import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { translate } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';

import { buttonStyle } from './constants';

const styles = {
  anchor: {
    textDecoration: 'none',
    color: 'inherit'
  },
  button: buttonStyle
};

class WebsiteButton extends React.Component {
  render() {
    const { t, href, target, classes } = this.props;
    return (
      <a href={href} target={target} className={classes.anchor}>
        <Button className={classes.button}>
          <i className="fas fa-globe"></i>&nbsp;&nbsp;{t('website')}
        </Button>
      </a>
    );
  }
}
WebsiteButton.propTypes = {
  t: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  href: PropTypes.string.isRequired,
  target: PropTypes.string
}
WebsiteButton.defaultProps = {
  target: '_blank'
}

export default compose(
  translate(['common']),
  withStyles(styles)
)(WebsiteButton);