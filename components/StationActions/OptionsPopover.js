import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { translate } from 'react-i18next';

import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';

import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import { buttonStyle } from './constants';


/**
 * Options component picks up the current station data to render available options inside the popover
 * ```javascript
 * <OptionsPopover
 *    reportLink="mailto:office@mail.com" editable={true} />
 * ```
 */
class OptionsPopover extends React.Component {
  static propTypes = {
    /**
     * Link userd by report handler method
     */
    reportLink: PropTypes.string,
    /**
     * Does current user has editing permissions
     */
    editable: PropTypes.bool
  }
  state = {
    isOpen: false,
    anchorEl: null,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'right'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    }
  }

  handleOpen = () => this.setState({
    isOpen: true,
    anchorEl: findDOMNode(this.button)
  });
  handleClose = () => this.setState({
    isOpen: false
  })

  handleReport(link) {
    window && window.open(link, '_blank');
  }

  render() {
    const { isOpen, anchorEl, anchorOrigin, transformOrigin } = this.state;
    const { t } = this.props;

    return (
      <React.Fragment>
        <Button style={buttonStyle}
          ref={node => this.button = node}
          onClick={this.handleOpen}>
          <i className="fas fa-ellipsis-v"></i>
        </Button>
        <Popover
          open={isOpen}
          anchorEl={anchorEl}
          anchorReference="anchorEl"
          onClose={this.handleClose}
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}>
          <List component="nav">
            <ListItem button dense onClick={this.handleReport.bind(null, 'mailto:test@mail.com')}>
              <ListItemIcon>
                <i className="fas fa-exclamation-triangle fa-fw"></i>
              </ListItemIcon>
              <ListItemText primary={t('report')} />
            </ListItem>
            <ListItem button dense onClick={this.handleReport.bind(null, 'mailto:marija.marinkovic@eutelnet.com')}>
              <ListItemIcon>
                <i className="fas fa-pencil-alt fa-fw"></i>
              </ListItemIcon>
              <ListItemText primary={t('edit')} />
            </ListItem>
          </List>
        </Popover>
      </React.Fragment>
    );
  }
}

export default translate(['common'])(OptionsPopover);

