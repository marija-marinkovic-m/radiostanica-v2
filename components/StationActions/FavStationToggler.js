import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { translate } from 'react-i18next';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { getFavorites } from '../../core/favorites/selectors';
import { toggleFavorite as toggleFavoriteAction } from '../../core/favorites/actions';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import Portal from '@material-ui/core/Portal';
import { buttonStyle } from './constants';

/**
 * Update the station's fav state based on the local storage data
 * ```javascript
 *  <FavoriteToggler
 *      station={{
 *        id: number;
 *        ...
 *      }}
 * />
 * ```
 */
class FavoriteToggler extends React.Component {

  static propTypes = {
    /**
     * Station object
     */
    station: PropTypes.object.isRequired,
    /**
     * List of all favorites from store
     */
    favorites: PropTypes.object,
    /**
     * Method for translations
     */
    t: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      snackBarOpen: false,
      snackBarMessage: '',
      icon: this.isFavorite(props) ? 'fas' : 'far'
    }
  }

  componentDidUpdate(prevProps) {
    // reset icon
    // open snackbar
    if (this.props.station.id !== prevProps.station.id) {
      return this.setState({
        icon: this.isFavorite(this.props) ? 'fas' : 'far'
      });
    }
    
    // if (!this._mounted) return;

    const prevIsFavorite = this.isFavorite(prevProps);
    const nextIsFavorite = this.isFavorite(this.props);

    if (prevIsFavorite === nextIsFavorite) return;

    this.setState({
      snackBarOpen: Boolean(this._mounted),
      snackBarMessage: nextIsFavorite ? 'saved to favorites' : 'removed from favorites',
      icon: nextIsFavorite ? 'fas' : 'far'
    });
  }

  handleToggleFavorite = () => {
    this._mounted = true;
    this.props.toggleFavorite(this.props.station);
  }

  render() {
    const { icon, snackBarOpen, snackBarMessage } = this.state;
    const { t } = this.props;

    return (
      <span>
        <Button
          onClick={this.handleToggleFavorite}
          aria-label="Toggle Favorite"
          style={buttonStyle}>
          <i className={cn([icon, 'fa-star'])}></i>
          &nbsp;&nbsp;{t('favorite')}
        </Button>
        <Portal>
        <Snackbar
          open={snackBarOpen}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          autoHideDuration={6000}
          onClose={this.handleCloseSnackBar}
          ContentProps={{
            'aria-describedby': 'favorites-message'
          }}
          message={<span id="favorites-message">{t(snackBarMessage)}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.handleCloseSnackBar}>
              <i className="fas fa-times"></i>
            </IconButton>
          ]} />
        </Portal>
      </span>
    );
  }

  handleCloseSnackBar = (event, reason) => {
    if (reason === 'clickaway') return;
    this.setState({snackBarOpen: false});
  }

  isFavorite = (props) => props.favorites &&
    props.station &&
    Object.keys(props.favorites)
      .map(f => String(f))
      .indexOf(String(props.station.id)) > -1;
}

const mapStateToProps = createSelector(
  getFavorites,
  (favorites) => ({ favorites })
);

const mapDispatchToProps = dispatchEvent => ({
  toggleFavorite: bindActionCreators(toggleFavoriteAction, dispatchEvent)
});

const enhance = compose(
  translate(['common']),
  connect(mapStateToProps, mapDispatchToProps)
);

export default enhance(FavoriteToggler);