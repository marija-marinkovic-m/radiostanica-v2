import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { translate } from 'react-i18next';

import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';

import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';

import { social as links, buttonStyle } from './constants';

const sharer = [
  {id: 'twitter', icon: 'fa-fw fab fa-twitter'},
  {id: 'facebook', icon: 'fa-fw fab fa-facebook'},
  {id: 'google', icon: 'fa-fw fab fa-google'},
  {id: 'tumblr', icon: 'fa-fw fab fa-tumblr'},
  {id: 'reddit', icon: 'fa-fw fab fa-reddit'},
  {id: 'whatsapp', icon: 'fa-fw fab fa-whatsapp', label: 'WhatsApp'},
  {id: 'mail', icon: 'fa-fw fas fa-envelope-square', msg: 'Slusam radio stanicu'},
  {id: 'pinterest', icon: 'fa-fw fab fa-pinterest'},
  {id: 'linkedin', icon: 'fa-fw fab fa-linkedin', label: 'LinkedIn'}
];

class ShareButton extends React.Component {
  state = {
    isOpen: false,
    anchorEl: null,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'left'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'left'
    }
  }

  handleOpen = () => this.setState({
    isOpen: true,
    anchorEl: findDOMNode(this.button)
  });
  handleClose = () => this.setState({
    isOpen: false
  })

  handleShare(n, msg) {
    window.open(links[n](location.href, msg), '_blank', 'noopener,dialog');
  }

  render() {
    const { t } = this.props;
    const { isOpen, anchorEl, anchorOrigin, transformOrigin } = this.state;
    return (
      <React.Fragment>
        <Button style={buttonStyle}
          ref={node => this.button = node}
          onClick={this.handleOpen}>
          <i className="fas fa-share"></i>&nbsp;&nbsp;{t('share')}
        </Button>
        <Popover
          open={isOpen}
          anchorEl={anchorEl}
          anchorReference="anchorEl"
          onClose={this.handleClose}
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}>
          <List component="nav">
            { sharer.map(n => <ListItem key={n.id} button dense onClick={this.handleShare.bind(null, n.id, (n.msg || 'Radiostanica.com'))}>
              <ListItemIcon>
                <i className={n.icon}></i>
              </ListItemIcon>
              <ListItemText primary={n.label || n.id} style={{textTransform: 'capitalize'}} />
            </ListItem>) }
          </List>
        </Popover>
      </React.Fragment>
    );
  }
}
ShareButton.propTypes = {
  t: PropTypes.func.isRequired
}

export default translate(['common'])(ShareButton);