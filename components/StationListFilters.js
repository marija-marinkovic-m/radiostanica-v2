import React from 'react';
import PropTypes from 'prop-types';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import cn from 'classnames';

import { Link } from '../routes';

import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';

import { withStyles } from '@material-ui/core/styles';
import { getResourceItems } from '../core/api/selectors';
import { fetchApiResource } from '../core/api/actions';
import { MOBILE_BREAKPOINT } from '../core/constants';

const styles = {
  root: {
    marginBottom: '15px',
    [MOBILE_BREAKPOINT]: {
      paddingLeft: '15px', paddingRight: '15px'
    }
  },
  popover: {
    padding: 0,
    display: 'flex'
  },
  menu: {
    listStyle: 'none',
    flexGrow: 1, flexBasis: 0,
    margin: '16px 0', padding: '0 14px',
    maxHeight: '400px', minWidth: '220px', overflow: 'auto',
    '& a': {
      display: 'block',
      padding: '2px 15px',
      fontSize: '16px', lineHeight: '28px',
      textDecoration: 'none', color: '#404854',
      '&.active': {
        fontWeight: 'bold', color: '#000',
        backgroundColor: '#F3F3F3'
      },
      '&:hover': {
        backgroundColor: '#F3F3F3'
      }
    },
    [MOBILE_BREAKPOINT]: {
      minWidth: 0
    }
  },
  button: {
    marginBottom: '10px', marginRight: '30px', padding: '6px', marginLeft: '-6px',
    color: '#373A3C', fontSize: '22px', textTransform: 'none',
    '& i': {
      marginLeft: '8px',
      fontSize: '12px', color: '#D6D6D6'
    },
    [MOBILE_BREAKPOINT]: {
      fontSize: '20px'
    }
  },
  divider: {
    display: 'block',
    height: '3px',
    margin: '15px 0',
    backgroundColor: '#EAEAEA'
  },
  borderMenu: {
    borderLeft: '3px solid #EAEAEA'
  }
};

class Filters extends React.PureComponent {
  state = {
    nationalityEl: null,
    genreEl: null
  }

  constructor(props) {
    super(props);
    const resources = ['nationalities', 'cities', 'genres'];
    resources.forEach(resource => {
      if (!props[resource].length) {
        props.fetchResource(resource, {include: (resource === 'cities' ? 'nationality.lang' : resource === 'nationalities' ? 'lang' : null )})
      }
    });
  }

  handleClick = (anchor, event) => {
    this.setState({ [`${anchor}El`]: event.currentTarget });
  }
  handleClose = (anchor) => {
    this.setState({ [`${anchor}El`]: null });
  }

  renderLocations = () => {
    const { nationalityEl } = this.state;
    const { nationalities, cities, parentParams, classes, t } = this.props;

    if (!nationalities || !nationalities.length) return null;

    const currNationality = nationalities.filter(n => n.lang === parentParams.nationality);
    const locations = cities.filter(c => c.nationalityId && c.nationality === parentParams.nationality);
    const closeFn = this.handleClose.bind(null, 'nationality');

    return (<React.Fragment>
      <Button
        className={classes.button}
        aria-owns={nationalityEl ? 'nationality-menu' : null}
        aria-haspopup="true"
        onClick={this.handleClick.bind(null, 'nationality')}>
        {currNationality.length ? currNationality[0].name : t('choose_nationality')}&nbsp;<i className="fas fa-chevron-down"></i>
      </Button>
      <Popover
        id="nationality-menu"
        anchorEl={nationalityEl}
        open={Boolean(nationalityEl)}
        onClose={closeFn}
        classes={{paper: classes.popover}}>
        <ul className={classes.menu} onClick={closeFn}>
          { nationalities.map(n => <li key={n.id}>
            <Link route="list" params={Object.assign({}, parentParams, {
              nationality: n.lang, strana: null, lokacija: null
            })}>
              <a className={cn({active: n.lang === parentParams.nationality})}>{n.name}</a>
            </Link>
          </li>) }
        </ul>

        { !currNationality.length ? null : (<ul className={cn([classes.menu, classes.borderMenu])} onClick={closeFn}>
          <li>
            <Link route="list" params={Object.assign({}, parentParams, {
              lokacija: null, strana: null
            })}><a className={cn({active: !parentParams.lokacija})}>{t('all_locations')}</a></Link>
          </li>
          { locations.map(l => <li key={l.id}>
            <Link route="list" params={Object.assign({}, parentParams, {
              lokacija: l.slug, strana: null
            })}><a className={cn({active: l.slug === parentParams.lokacija})}>{l.name}</a></Link>
          </li>) }
          </ul>) }
      </Popover>
    </React.Fragment>);
  }

  renderGenres = () => {
    const { genreEl } = this.state;
    const { genres, parentParams, classes, t } = this.props;

    if (!genres.length) return null;

    const currGenre = genres.filter(g => g.slug === parentParams.zanr);
    const route = parentParams.nationality ? 'list' : 'radio-stanice';
    const closeFn = this.handleClose.bind(null, 'genre');

    return (<React.Fragment>
      <Button
        className={classes.button}
        aria-owns={genreEl ? 'genre-menu' : null}
        aria-haspopup="true"
        onClick={this.handleClick.bind(null, 'genre')}>
        {currGenre.length ? currGenre[0].name : t('popular')}&nbsp;<i className="fas fa-chevron-down"></i>
      </Button>
      <Popover
        id="genre-menu"
        anchorEl={genreEl}
        open={Boolean(genreEl)}
        onClose={closeFn}>
        <ul className={classes.menu} onClick={closeFn}>
          <li>
            <Link route={route} params={Object.assign({}, parentParams, {zanr: null, strana: null})}>
              <a className={cn({active: !parentParams.zanr})}>{ t('popular') }</a>
            </Link>
          </li>
          <li className={classes.divider}></li>
          { genres.map(g => <li key={g.id}>
            <Link route={route} params={Object.assign({}, parentParams, {
              zanr: g.slug, strana: null
            })}><a className={cn({active: g.slug === parentParams.zanr})}>{g.name}</a></Link>
          </li>) } 
        </ul>
      </Popover>
    </React.Fragment>);
  }

  render() {
    const Locations = this.renderLocations;
    const Genres = this.renderGenres;
    
    return (<div className={this.props.classes.root}>
      <Locations />
      <Genres />
    </div>);
  }

}

Filters.propTypes = {
  parentParams: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  nationalities: getResourceItems(state, 'nationalities'),
  cities: getResourceItems(state, 'cities'),
  genres: getResourceItems(state, 'genres')
});
const mapDispatchToProps = (dispatch) => ({
  fetchResource: bindActionCreators(fetchApiResource, dispatch)
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles),
  translate(['common'])
)(Filters);