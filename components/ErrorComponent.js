import React from 'react';
import { translate } from 'react-i18next';

/**
 * This component is used for when we don't have specific reason for request failure
 * ```javascript
 * // usage
 * <ErrorComponent />
 * ```
 */
const ErrorComponent = ({t}) => (
  <h3>{t('something_went_wrong')}</h3>
);

export default translate(['common'])(ErrorComponent);