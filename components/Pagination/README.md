# Props

| Name                      | Type        | Description                                                                                   |
| ---                       | ---         | ---                                                                                           |
| `pageCount`               | `Number`    | **Required.** The total number of pages.                                                      |
| `pageRangeDisplayed`      | `Number`    | **Required.** The range of pages displayed.                                                   |
| `marginPagesDisplayed`    | `Number`    | **Required.** The number of pages to display for margins.                                     |
| `previousLabel`           | `Node`      | Label for the `previous` button.                                                              |
| `nextLabel`               | `Node`      | Label for the `next` button.                                                                  |
| `breakLabel`              | `Node`      | Label for ellipsis.                                                                           |
| `onPageChange`            | `Function`  | The method to call when a page is clicked. Exposes the current page object as an argument.    |
| `initialPage`             | `Number`    | The initial page selected.                                                                    |
| `forcePage`               | `Number`    | To override selected page with parent prop.                                                   |
| `disableInitialCallback`  | `boolean`   | Disable `onPageChange` callback with initial page. Default: `false`                           |
| `hrefBuilder`             | `Function`  | The method is called to generate the `href` attribute value on tag `a` of each page element.  |
| `extraAriaContext`        | `String`    | Extra context to add to the `aria-label` HTML attribute.                                      |

**Credits &copy; [https://github.com/AdeleD](https://github.com/AdeleD)**