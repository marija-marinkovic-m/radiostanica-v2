import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { compose } from 'redux';
import { translate } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';

import Page from './PaginationSubElements/Page';
import Break from './PaginationSubElements/Break';
import { MOBILE_BREAKPOINT } from '../../core/constants';

const styles = (theme) => ({
  root: {
    listStyle: 'none',
    display: 'flex',
    justifyContent: 'center', alignItems: 'center',
    margin: '25px 0', padding: 0,
    color: '#55ACEE', fontSize: '14px', lineHeight: '22px',
    letterSpacing: '1px',
    '& > .disabled': {
      color: '#a9a2a2'
    },
    '& > .prev, & > .next': {
      letterSpacing: 'normal',
      marginLeft: '12px', marginRight: '12px',
      '& span': {
        [MOBILE_BREAKPOINT]: {
          display: 'none'
        }
      }
    },
    '& a': {
      cursor: 'pointer'
    }
  }
});

/**
 * Component to render pagination
 * ```javascript
 * // usage
 * <Pagination
 *    pageCount={12}
 *    pageRangeDisplayed={5}
 *    marginPagesDisplayed={1}
 *    initialPage={0}
 *    onPageChange={this.onPageChange}
 * />
 * ```
 */
class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.initialPage ? props.initialPage : props.forcePage ? props.forcePage : 0
    }
  }

  componentDidMount() {
    const { initialPage, disableInitialCallback } = this.props;
    if (typeof(initialPage) !== 'undefined' && !disableInitialCallback) {
      this.callCallback(initialPage);
    }
  }

  componentDidUpdate(prevProps) {
    if (typeof(this.props.forcePage) !== 'undefined' && this.props.forcePage !== prevProps.forcePage) {
      this.setState({selected: this.props.forcePage});
    }
  }

  handlePreviousPage = evt => {
    const { selected } = this.state;
    evt.preventDefault ? evt.preventDefault() : (evt.returnValue = false);
    if (selected > 0) {
      this.handlePageSelected(selected - 1, evt);
    }
  };

  handleNextPage = evt => {
    const { selected } = this.state;
    const { pageCount } = this.props;

    evt.preventDefault ? evt.preventDefault() : (evt.returnValue = false);
    if (selected < pageCount - 1) {
      this.handlePageSelected(selected + 1, evt);
    }
  };

  handlePageSelected = (selected, evt) => {
    evt.preventDefault ? evt.preventDefault() : (evt.returnValue = false);

    if (this.state.selected === selected) return;

    this.setState({selected: selected});

    // Call the callback with the new selected item:
    this.callCallback(selected);
  };

  hrefBuilder(pageIndex) {
    const { hrefBuilder, pageCount } = this.props;
    if (hrefBuilder &&
      pageIndex !== this.state.selected &&
      pageIndex >= 0 &&
      pageIndex < pageCount
    ) {
      return hrefBuilder(pageIndex + 1);
    }
  }

  callCallback = (selectedItem) => {
    if (typeof(this.props.onPageChange) !== "undefined" &&
        typeof(this.props.onPageChange) === "function") {
      this.props.onPageChange({selected: selectedItem});
    }
  };

  getPageElement(index) {
    const { selected } = this.state;
    const {
      extraAriaContext
    } = this.props;

    return <Page
      key={index}
      onClick={this.handlePageSelected.bind(null, index)}
      selected={selected === index}
      extraAriaContext={extraAriaContext}
      href={this.hrefBuilder(index)}
      page={index + 1} />
  }

  pagination = () => {
    const items = [];
    const {
      pageRangeDisplayed,
      pageCount,
      marginPagesDisplayed,
      breakLabel
    } = this.props;

    const { selected } = this.state;

    if (pageCount <= pageRangeDisplayed) {

      for (let index = 0; index < pageCount; index++) {
        items.push(this.getPageElement(index));
      }

    } else {

      let leftSide  = (pageRangeDisplayed / 2);
      let rightSide = (pageRangeDisplayed - leftSide);

      if (selected > pageCount - pageRangeDisplayed / 2) {
        rightSide = pageCount - selected;
        leftSide  = pageRangeDisplayed - rightSide;
      }
      else if (selected < pageRangeDisplayed / 2) {
        leftSide  = selected;
        rightSide = pageRangeDisplayed - leftSide;
      }

      let index;
      let page;
      let breakView;
      let createPageView = (index) => this.getPageElement(index);

      for (index = 0; index < pageCount; index++) {

        page = index + 1;

        if (page <= marginPagesDisplayed) {
          items.push(createPageView(index));
          continue;
        }

        if (page > pageCount - marginPagesDisplayed) {
          items.push(createPageView(index));
          continue;
        }

        if ((index >= selected - leftSide) && (index <= selected + rightSide)) {
          items.push(createPageView(index));
          continue;
        }

        if (breakLabel && items[items.length - 1] !== breakView) {
          breakView = (
            <Break
              key={index}
              label={breakLabel}
            />
          );
          items.push(breakView);
        }
      }
    }

    return items;
  };

  render() {
    const {
      pageCount,
      previousLabel,
      nextLabel,
      classes,
      t
    } = this.props;

    const { selected } = this.state;

    const prevDisabled = selected === 0;
    const nextDisabled = selected === pageCount - 1;

    return (
      <ul className={classes.root}>
        <li className={cn(['prev'], {disabled: prevDisabled})}>
          <a onClick={this.handlePreviousPage}
             href={this.hrefBuilder(selected - 1)}
             tabIndex="0"
             role="button"
             onKeyPress={this.handlePreviousPage}>
            &lsaquo;<span>&nbsp;&nbsp;{t(previousLabel)}</span>
          </a>
        </li>

        {this.pagination()}

        <li className={cn(['next'], {disabled: nextDisabled})}>
          <a onClick={this.handleNextPage}
             href={this.hrefBuilder(selected + 1)}
             tabIndex="0"
             role="button"
             onKeyPress={this.handleNextPage}>
            <span>{t(nextLabel)}&nbsp;&nbsp;</span>&rsaquo;
          </a>
        </li>
      </ul>
    );
  }
}

Pagination.propTypes = {
  /**
   * The total number of pages.
   */
  pageCount: PropTypes.number.isRequired,
  /**
   * The range of pages displayed.
   */
  pageRangeDisplayed: PropTypes.number.isRequired,
  /**
   * The number of pages to display for margins
   */
  marginPagesDisplayed: PropTypes.number.isRequired,
  /**
   * Label for the `previous` button.
   */
  previousLabel: PropTypes.node,
  /**
   * Label for the `next` button.
   */
  nextLabel: PropTypes.node,
  /**
   * Label for ellipsis.
   */
  breakLabel: PropTypes.node,
  /**
   * The method is called to generate the `href` attribute value on tag `a` of each page element.
   */
  hrefBuilder: PropTypes.func,
  /**
   * The method to call when a page is clicked. Exposes the current page object as an argument.
   */
  onPageChange: PropTypes.func,
  /**
   * The initial page selected.
   */
  initialPage: PropTypes.number,
  /**
   * To override selected page with parent prop.
   */
  forcePage: PropTypes.number,
  /**
   * Disable `onPageChange` callback with initial page
   */
  disableInitialCallback: PropTypes.bool,
  /**
   * Classes to apply to pagination wrapper
   */
  classes: PropTypes.object.isRequired,
  /**
   * Translation method
   */
  t: PropTypes.func.isRequired
}

Pagination.defaultProps = {
  pageCount: 10,
  pageRangeDisplayed: 2,
  marginPagesDisplayed: 3,
  previousLabel: "previous",
  nextLabel: "next",
  breakLabel: "...",
  disableInitialCallback: false
}

export default compose(
  translate(['common']),
  withStyles(styles)
)(Pagination);