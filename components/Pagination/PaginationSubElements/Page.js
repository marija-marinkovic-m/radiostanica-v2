import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withStyles } from '@material-ui/core/styles';

const styles = {
  listItem: {
    margin: '0 3px'
  },
  anchor: {
    display: 'block', padding: '0 7px',
    borderRadius: '2px',
    color: '#55ACEE',
    '&.selected': {
      color: '#FFF',
      background: '#55ACEE'
    }
  }
}

const Page = ({onClick, href, extraAriaContent, selected, page, classes}) => {
  const anchorProps = {
    role: 'button',
    tabIndex: 0,
    onKeyPress: onClick,
    'aria-label': selected ? `Page ${page} is your current page` : `Page ${page} ${extraAriaContent}`,
    'aria-current': selected ? 'page' : null,
    onClick,
    href
  };

  return (
    <li className={classes.listItem}>
      <a className={cn([classes.anchor], {selected})} {...anchorProps}>{page}</a>
    </li>
  );
};

Page.propTypes = {
  selected: PropTypes.bool,
  extraAriaContent: PropTypes.string,
  href: PropTypes.string,
  page: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired
}
Page.defaultProps = {
  extraAriaContent: ''
}

export default withStyles(styles)(Page);