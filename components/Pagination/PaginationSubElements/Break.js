import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    letterSpacing: 0
  }
}

const Break = ({label, classes}) => (
  <li className={classes.root}>{label}</li>
);

Break.propTypes = {
  label: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Break);