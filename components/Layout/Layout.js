import React from 'react';
import Router from 'next/router';

import Header from './Header';
import Footer from './Footer';
import Player from '../Player';

import NProgress from '../../util/nprogress';

Router.onRouteChangeStart = url => {
  NProgress.start();
};
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

export default ({children, q}) => (
  <div className="layout">
    <Header />
    <main className="wrapper">{children}</main>
    <Footer />
    <Player query={q} />
  </div>
);