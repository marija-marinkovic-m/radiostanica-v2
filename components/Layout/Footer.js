import React from 'react';

const Footer = () => {
  const today = new Date();
  return (
    <footer>
      <p className="wrapper">&copy;&nbsp;2005-{today.getFullYear()} radiostanica.com</p>
    </footer>
  );
}
export default Footer;