import React from 'react';
import { connect } from 'react-redux';
import { Link } from '../../../routes';

import Search from './Search';
import Menu from './Menu';
import { DEFAULT_NATIONALITY_KEY } from '../../../core/constants';
import { getScreenWidth } from '../../../core/mediaQueryTracker/selectors';

/**
 * General Header description.
 * You can even use the native Markdown here.
 * E.g.:
 * ```html
 * <Header defaultNationality={541} />
 * ```
 */
const Header = ({defaultNationality, screenWidth}) => {
  return (
    <header>
      <div className="wrapper">
        <Link route={`/${defaultNationality}`}>
          <a className="logo">
            <img src="/static/assets/images/logo-landing.png" alt="Radiostanica.com Logo" />
            <i className="sr-only">Radiostanice.com</i>
          </a>
        </Link>

        { Boolean(screenWidth) && (screenWidth <= 768 ? null : <Search />) }

        <Menu />
      </div>
    </header>
  );
};

export default connect((state) => ({
  defaultNationality: state.cookies[DEFAULT_NATIONALITY_KEY],
  screenWidth: getScreenWidth(state)
}))(Header);

