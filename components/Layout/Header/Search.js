import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { translate } from 'react-i18next';
import { Router } from '../../../routes';
import cn from 'classnames';

import Downshift from 'downshift';
import { Transition } from 'react-spring';

import debounce from '../../../util/debounce';
import { api } from '../../../core/api/api-service';
import { createStation as normalize } from '../../../core/stations/reducer';
import ImgTag from '../../shared/ImgTag';
import PlayButton from '../../shared/PlayButton';

import makeCancelable from '../../../util/cancelable-promise';

const debounceTime = 300;
const perPage = 5;

class Search extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      locations: [],
      loading: false,
      error: null
    }

    this._searchInputRef = React.createRef();
  }

  componentDidMount() {
    this._isMounted = true;
    this.cleanup = () => {
      this._searchInputRef.current.value = '';
      
      this._isMounted = false;
      this._promise && this._promise.cancel();
    }
    this._searchInputRef.current.focus();
  }

  componentWillUnmount() {
    console.log('called cleanup');
    this.cleanup();
  }

  fetchResults = debounce(name => {
    if (!this._isMounted) return;
    this.setState({loading: true}, () => {
      this._promise = makeCancelable(Promise.all([
          api.fetchStations({name, perPage, include: 'nationality.lang', active: 'active_station'}),
          api.fetchResources('cities', {name, perPage, include: 'nationality.lang'})
        ]));
      
      this._promise.promise
        .then(this.handleResults)
        .catch(this.handleFetchError);
    });
  }, debounceTime);

  handleResults = ([{collection}, {data}]) => {
    if (!this._isMounted) return;
    this.setState({
      loading: false,
      items: collection.map(s => normalize(s)),
      locations: data.filter(l => l.nationalityId)
    });
  }

  handleFetchError = (err) => {
    if (!this._isMounted) return;
    this.setState({
      loading: false,
      items: [],
      error: err
    });
  }

  handleRoute = (route, e) => {
    if (e) e.preventDefault();
    // this.cleanup();
    Router.pushRoute(route);
    this.props.onRoute && this.props.onRoute();
  }

  handleDownshiftSelect = (selection) => {
    if (!this._isMounted) return;
    console.log(selection);
    // determine is it station or location
    // artworkUrl and stream props should be station indicators
    let route = '/';
    if ('artworkUrl' in selection || 'stream' in selection) {
      // it is station
      route = `/radio/${selection.id}/${selection.slug}`;
    } else {
      route = `/${selection.nationality}/radio-stanice?lokacija=${selection.slug}`
    }
    this.handleRoute(route);
  }

  handleInputChange = ({ target: { value } }) => {
    if (!this._isMounted) return;
    if (value) this.fetchResults(value);
  }

  renderDownshift = ({selectedItem, getInputProps, getItemProps, highlightedIndex, isOpen}) => {
    const { loading, items, locations } = this.state;
    const { t } = this.props;

    const inputProps = getInputProps({
      id: 'radio-app-input-search',
      placeholder: t('search'),
      ref: this._searchInputRef,
      onChange: this.handleInputChange
    });

    return (
      <div className="container">
        <input {...inputProps} />
        <button className="search-btn" type="button" onClick={e => e.preventDefault()} disabled={loading}>
          <i className={cn(['fas'], {'fa-search': !loading, 'fa-spinner-third fa-spin': loading})}></i>
        </button>

        { isOpen && <div className="results">

          {/* STATIONS */}
          { items.length > 0 && <div className="headline">{t('stations')}</div> }
          <Transition
            keys={items.map(item => item.id)}
            from={{opacity: 0}}
            enter={{opacity: 1}}
            leave={{opacity: 0}}>
          { items.map((item, index) => tStyles => {
            const isHighlighted = highlightedIndex === index;
            const itemProps = getItemProps({
              item,
              style: {
                backgroundColor: isHighlighted ? '#F1F1F1' : 'white',
                ...tStyles
              },
              className: 'item',
              onClick: this.handleRoute.bind(null, `/radio/${item.id}/${item.slug}`)
            });

            return (
              <div {...itemProps}>

                <div className="content">
                  <ImgTag
                    src={item.artworkUrl}
                    render={src => <figure style={{backgroundImage: `url(${src})`}} />}
                  />
                  <div className="info">
                    <h2>{item.title}</h2>
                    <h4>{item.city}</h4>
                    <p>{ item.genres && item.genres.map(g => g.name).reduce((a,n) => [a, ', ', n]) }</p>
                  </div>
                </div>

                <PlayButton
                  size="sm"
                  station={item}
                />
              </div>
            );
          }) }
          </Transition>

          {/* LOCATIONS */}
          { locations.length > 0 && <div className="headline">{t('locations')}</div> }
          <Transition
            keys={locations.map(location => location.id)}
            from={{opacity: 0}}
            enter={{opacity: 1}}
            leave={{opacity: 0}}>
          { locations.map((location, index) => tStyles => {
            const isHighlighted = highlightedIndex === this.state.items.length + index;
            const locationProps = getItemProps({
              item: location,
              style: {
                backgroundColor: isHighlighted ? '#F3F3F3' : 'white',
                ...tStyles
              },
              className: 'item location',
              onClick: this.handleRoute.bind(null, `/${location.nationality}/radio-stanice?lokacija=${location.slug}`)
            });

            return (
              <div
                {...locationProps}>
                <h3>{location.name}</h3>
                <i className="fas fa-chevron-right"></i>
              </div>
            );
          }) }
          </Transition>
        </div> }
      </div>
    );
  }

  render() {
    const downshiftProps = {
      render: this.renderDownshift,
      itemToString: s => s == null ? '' : s.title || s.name,
      onChange: this.handleDownshiftSelect
    };
    if (this.props.isOpen) {
      downshiftProps.isOpen = true;
    }
    return <div className="search-wrap">
      <Downshift {...downshiftProps} />
    </div>
  }
}

Search.propTypes = {
  onRoute: PropTypes.func,
  isOpen: PropTypes.bool,  
  t: PropTypes.func.isRequired,
}

export default compose(
  translate(['common'])
)(Search);