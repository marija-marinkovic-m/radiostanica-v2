import React from 'react';
import { Link } from '../../../routes';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'next/router';
import { DEFAULT_NATIONALITY_KEY } from '../../../core/constants';
import { getScreenWidth } from '../../../core/mediaQueryTracker/selectors';

import Modal from '@material-ui/core/Modal';
import Search from './Search';

class Menu extends React.PureComponent {
  state = {
    searchOpen: false
  };
  toggleSearch = (e) => {
    if (e) e.preventDefault();
    this.setState((prevState) => ({searchOpen: !prevState.searchOpen}));
  }
  render() {
    const { defaultNationality, screenWidth, router } = this.props;
    return (<React.Fragment>
      <ul className="menu">
        { Boolean(screenWidth && screenWidth <= 768) && <li>
          <a href="#search" onClick={this.toggleSearch} style={{color: 'inherit'}}>
            <i className="fas fa-search"></i>
            <span className="sr-only">Search</span>
          </a>
        </li> }
        { defaultNationality && <li>
          <Link route={`/${defaultNationality}/radio-stanice`}>
            <a style={{color: router.asPath === `/${defaultNationality}/radio-stanice` ? '#55ACEE' : 'inherit'}}>
              <i className="fas fa-th-list"></i>
              <span className="sr-only">List</span>
            </a>
          </Link>
        </li> }
        <li>
          <Link route="/favorites">
            <a style={{color: router.asPath === '/favorites' ? '#55ACEE' : 'inherit'}}>
              <i className="fas fa-star"></i>
              <span className="sr-only">Favorites</span>
            </a>
          </Link>
        </li>
      </ul>

      { Boolean(screenWidth && screenWidth <= 768) && <Modal open={this.state.searchOpen} onClose={this.toggleSearch}>
          <div className="modal-placeholder">
            <ul className="menu">
              <li>
              <Link route={`/${defaultNationality}`}>
                <a className="logo">
                  <img src="/static/assets/images/logo-landing.png" alt="Radiostanica.com Logo" />
                </a>
              </Link>
              </li>
              <li>
                <button
                  className="close-btn"
                  onClick={this.toggleSearch}>
                  <i className="fas fa-times"></i>
                </button>
              </li>
            </ul>
            <Search
              isOpen
              onRoute={this.toggleSearch}
            />
          </div>
      </Modal> }
    </React.Fragment>);
  }
}

export default compose(
  connect((state) => ({
    defaultNationality: state.cookies[DEFAULT_NATIONALITY_KEY],
    screenWidth: getScreenWidth(state)
  })),
  withRouter
)(Menu);