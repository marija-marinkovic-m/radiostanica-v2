const next = require('next');
const routes = require('./routes');

const fs = require('fs');

const { parse } = require('url');
const { join } = require('path');

const port = parseInt(process.env.PORT, 10) || 3000;
const app = next({dev: process.env.NODE_ENV !== 'production'});
const appHandler = routes.getRequestHandler(app);
const express = require('express');

const csvdata = require('./util/csv-data');

const es6promise = require('es6-promise');
require('isomorphic-unfetch');
es6promise.polyfill();

const handler = routes.getRequestHandler(app, ({req, res, route, query}) => {
  const parsedUrl = parse(req.url);
  const rootStaticFiles = [
    '/robots.txt',
    '/sitemap.xml',
    '/favicon.ico',
    '/admin.html'
  ];

  if (rootStaticFiles.indexOf(parsedUrl.pathname) > -1) {
    const path = parsedUrl.pathname !== '/admin.html'
      ? join(__dirname, 'static', parsedUrl.pathname)
      : join(__dirname, 'static/admin/build/index.html');
    app.serveStatic(req, res, path);
  } else {

    if (req.body) {
      if (req.body.requestUrl) {
        const {
          body: {
            requestUrl,
            method = 'GET',
            headers = {
              'Content-Type': 'application/json'
            },
            body = null
          }
        } = req;
        return fetch(requestUrl, {
            method,
            headers,
            body
          })
          .then(data => data.json())
          .then(json => res.json(json));
        // .catch(err => res.status(500).json(err));
      } else if (req.body.streamLog && req.body.data) {
        const filePath = `./static/logs/${req.body.fileName}.csv`;
        return csvdata.load(filePath)
          .then(loadedData => {
            const theData = loadedData.concat(req.body.data);
            _writeData(filePath, theData);
          })
          .catch(_ => {
            // file not found / error loading
            _writeData(filePath, req.body.data);
          });
      } else if (req.body.fetchLogs && req.body.path) {
        const files = fs.readdirSync(req.body.path);
        return res.send(files);
      }
    }

    appHandler(req, res, route, query)
  }
})

app.prepare().then(() => {
  express()
    .use(express.json())
    .use(handler)
    .listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    })
});

function _writeData (filePath, theData) {
  csvdata.write(filePath, theData, {
    header: 'time,station,message'
  })
  .then(success => console.log(success))
  .catch(err => console.log(err));
}