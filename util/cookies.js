const serverCookie = require('./cookie-server');

export function setCookie(ctx, key, value, days) {
  const date = new Date();
  date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

  if (ctx && ctx.res) {
    // server
    ctx.res.setHeader('Set-Cookie', serverCookie.serialize(key, value, {
      maxAge: date.getTime(),
      path: '/'
    }));
  } else {
    // client
    require('./cookie-client')(key, value, {
      maxage: date.getTime(),
      path: '/'
    });
  }
}

export function parseCookie(ctx, options = {}) {
  if (ctx && ctx.req) {
    // server
    const cookies = ctx.req.headers.cookie;
    if (!cookies) return {};
    return serverCookie.parse(cookies, options);
  } else {
    // client
    return require('./cookie-client')();
  }
}