import qs from 'query-string';
import { PLAYER_QUERY_VAR } from '../core/constants';
/**
 * getLinkHref util
 * @param {query} prevQuery current router query object
 * @param {nextPathname} nextPathname link to page pathname
 * @param {nextQuery} nextQuery link to page query
 */
function getLinkHref(prevQuery, nextPathname = '/', nextQuery = {}) {
  const query = prevQuery && prevQuery[PLAYER_QUERY_VAR] ?
    Object.assign({}, { [PLAYER_QUERY_VAR]: 1 }, nextQuery) :
    nextQuery;
  const pathname = encodeURI(nextPathname);
  const asPath = Object.keys(nextQuery).length > 0 ? pathname + '?' + qs.stringify(nextQuery) : pathname;
  return {
    pathname,
    query,
    asPath
  }
}

export default getLinkHref;