const routes = module.exports = require('next-routes')();

routes
  .add('station', '/radio/:id/:slug?')
  .add('radio-stanice', '/radio-stanice', '/list')
  .add('favorites', '/favorites', '/favorites')
  .add('login', '/login', '/login')
  .add('/:nationality?', '/')
  .add('list', '/:nationality/radio-stanice');